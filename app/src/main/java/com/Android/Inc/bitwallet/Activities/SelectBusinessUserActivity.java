package com.Android.Inc.bitwallet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomToast;

import java.util.concurrent.ScheduledExecutorService;

public class SelectBusinessUserActivity extends AppCompatActivity implements View.OnClickListener{


    private Button btnAdmin,btnEmployee;
    private Context context;
    private LinearLayout ll_drawer;
    String screenType;
    private boolean minimizeBtnPressed = false;
    private static int TIME_OUT = 120;
    private View  layout;
    private static final String TAG = SelectBusinessUserActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_select_business_user_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_select_business_user);
        }
        context = SelectBusinessUserActivity.this;
        initialize();
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        boolean  isEmpSwtchOn = ACU.MySP.getSPBoolean(context,ACU.MySP.IS_EMP_SWITCH_ON,false);
        Log.e(TAG, "onCreate: "+isEmpSwtchOn );
        if (isEmpSwtchOn){
            btnEmployee.setVisibility(View.VISIBLE);
        }else{
            btnEmployee.setVisibility(View.GONE);
        }
    }


    private void initialize() {
        btnAdmin = findViewById(R.id.btn_admin);
        btnEmployee = findViewById(R.id.btn_employee);

        btnAdmin.setOnClickListener(this);
        btnEmployee.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_admin:
                ACU.MySP.saveSP(context,ACU.MySP.BUSSINESS_USER_TYPE,"admin");
                startActivity(new Intent(context, PinActivity.class));
                finish();
                break;

            case R.id.btn_employee:
                ACU.MySP.saveSP(context,ACU.MySP.BUSSINESS_USER_TYPE,"employee");
                dialogSingleLine("This Feature Is Not Yet Available");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            Log.e(TAG, "doExitApp: onBackPressed");
            minimizeBtnPressed = true;
            finish();
        }
    }

    public void dialogSingleLine(String text) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            if (screenType.equalsIgnoreCase("tablet")) {
                dialog.setContentView(R.layout.dialog_one_line_text_tab);
            } else {
                dialog.setContentView(R.layout.dialog_one_line_text);
            }
            TextView msgTxt = dialog.findViewById(R.id.txt_msg);
            msgTxt.setText(text);

            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
