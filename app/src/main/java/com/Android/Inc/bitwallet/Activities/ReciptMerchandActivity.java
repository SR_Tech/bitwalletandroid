package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

public class ReciptMerchandActivity extends AppCompatActivity implements View.OnClickListener {

    Context context;
    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    Button btnGetReceipt, btnNoThanks;
    private ProgressBar progressBar;
    private String status_code = "";
    private boolean status;
    private View layout;
    String strTimeZone = null;
    String screenType;
    private boolean minimizeBtnPressed = false;
    private API exampleApi;
    private static final String TAG = ReciptMerchandActivity.class.getSimpleName();
    public static String receiptUrl = "business/v2/sendreceipt"; //business/v2/sendreceipt
    private RestAsyncTask restAsyncTask = null;
    private Dialog dialogReceipt;
    private String senderAddress, strHash, strTimeStamp, strReceiverAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_recipt_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_recipt);
        }

        context = ReciptMerchandActivity.this;
        initialize();
        getIntentData();
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
    }

   /*//Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
      *//*  if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
          //  finish();
        }*//*
        super.onUserLeaveHint();
    }*/

    @Override
    protected void onResume() {
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        super.onResume();
    }

    /*@Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }*/

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            senderAddress = extras.getString("senderAddress");
            strHash = extras.getString("hash");
            strTimeStamp = extras.getString("timeStamp");
            strReceiverAddress = extras.getString("receiverAddress");


        }

    }


    private void initialize() {

        btnGetReceipt = findViewById(R.id.btn_get_receipt);
        btnNoThanks = findViewById(R.id.btn_no_thanks);
        progressBar = findViewById(R.id.receipt_progressBar);

//        imgFaceboook = findViewById(R.id.img_facebook);
//        imgTwitter = findViewById(R.id.img_twitter);
//        imgLinkedIn = findViewById(R.id.img_linkedIn);
//
//        imgFaceboook.setOnClickListener(this);
//        imgTwitter.setOnClickListener(this);
//        imgLinkedIn.setOnClickListener(this);
        btnGetReceipt.setOnClickListener(this);
        btnNoThanks.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_get_receipt:
                dialogReceipt();
                break;
            case R.id.btn_no_thanks:
                receiptData("0", 0);
                break;
//            case R.id.img_facebook:
//                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
//                break;
//            case R.id.img_linkedIn:
//                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
//                break;
//            case R.id.img_twitter:
//                socialMedia("twitter", "https://twitter.com/bitwalletinc");
//                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

        }
    }


    private void receiptData(String emailId, final int isreceiptsend) {

        final String encode_emailId = VU.encode(emailId.getBytes());
        final String encode_businessName = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.BUSINESS_NAME, "").toString().getBytes());
        final String encode_EmpEmail = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.EMP_EMAIL, "").toString().getBytes());
        final String encode_EmpCode = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.EMP_ID, "").toString().getBytes());
        final String encode_BusssinessEmail = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").toString().getBytes());
        final String encode_TipPercent = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.TIP_PERCENT, "").toString().getBytes());

        Calendar cal = Calendar.getInstance();
        long milliDiff = cal.get(Calendar.ZONE_OFFSET);
// Got local offset, now loop through available timezone id(s).
        String[] ids = TimeZone.getAvailableIDs();
        for (String id : ids) {
            TimeZone tz = TimeZone.getTimeZone(id);
            if (tz.getRawOffset() == milliDiff) {
                // Found a match.
                strTimeZone = id;
                break;
            }
        }
        Log.e(TAG, "receiptData: " + strTimeZone);
        String urlParameters = "businessName=" + encode_businessName + "&billAmt=" + ACU.MySP.getFromSP(context, ACU.MySP.TIP_RECEIPT_DOLLAR_AMT, "")
                + "&tipAmt=" + ACU.MySP.getFromSP(context, ACU.MySP.TIP_AMT, "") +
                "&receipentEmail=" + encode_emailId + "&timeZone=" + strTimeZone + "&txHash=" + strHash + "&senderAddr=" + senderAddress +
                "&receiverAddr=" + strReceiverAddress + "&timestamp=" + strTimeStamp +
                "&empcode=" + encode_EmpCode + "&empemail=" + encode_EmpEmail +
                "&merchantemail=" + encode_BusssinessEmail + "&tip_pct=" + encode_TipPercent + "&isreceiptsend=" + isreceiptsend;

        Log.e(TAG, "z: "+urlParameters );
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(receiptUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        status_code = jsonObject.getString("status_code");
                        status = jsonObject.getBoolean("status");
                        String msg = jsonObject.getString("message");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context,screenType);
                        }
                        if (isreceiptsend == 1) {
                            dialogReceiptSingleLine(msg);
                        }else{
                            startActivity(new Intent(context, ChargeBitwalletActivity.class));
                            finish();
                           // CustomDialogs.dialogShowMsg(context,screenType,msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, ChargeBitwalletActivity.class));
        finish();
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public void dialogReceiptSingleLine(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (status) {
                    startActivity(new Intent(context, ChargeBitwalletActivity.class));
                    finish();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogReceipt() {
        dialogReceipt = new Dialog(context, R.style.MaterialDialogSheet);
        dialogReceipt.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialogReceipt.setContentView(R.layout.dialog_receipt_tab);
        } else {
            dialogReceipt.setContentView(R.layout.dialog_receipt);
        }

        dialogReceipt.setCancelable(true);

        final AppCompatEditText edtEmail = dialogReceipt.findViewById(R.id.edt_email);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogReceipt.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialogReceipt.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!VU.isEmailId(edtEmail)) {
                    dialogReceipt.cancel();
                    receiptData(edtEmail.getText().toString().trim(), 1);
                    //  receiptRequest();
                } else {
                    CustomToast.custom_Toast(context, "Please Enter Email", layout);
                }
            }
        });
        dialogReceipt.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogReceipt.show();
        dialogReceipt.getWindow().setAttributes(lp);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (restAsyncTask != null && restAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            restAsyncTask.cancel(true);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (restAsyncTask != null && restAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            restAsyncTask.cancel(true);

        }
    }
}


