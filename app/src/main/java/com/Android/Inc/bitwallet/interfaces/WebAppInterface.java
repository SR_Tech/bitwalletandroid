package com.Android.Inc.bitwallet.interfaces;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.CreatepaymentmethodActivity;
import com.Android.Inc.bitwallet.Activities.BuySellActivity.SelectDocumentForUpload;
import com.Android.Inc.bitwallet.Activities.BuySellActivity.SellActivity;
import com.Android.Inc.bitwallet.Activities.TransactionActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class WebAppInterface {
    Context mContext;
    String publicToken, strWyreAccid;

    public WebAppInterface(Context ctx, String strWyreAccid) {
        this.mContext = ctx;
        this.strWyreAccid = strWyreAccid;}


    @JavascriptInterface
    public void sendData(String data) {
        //Get the string value to process

        this.publicToken = data;
        Log.e("WebAppInterface", "sendData: " + publicToken);
        Intent intent = new Intent(mContext, CreatepaymentmethodActivity.class);
        intent.putExtra("publicToken",publicToken);
        intent.putExtra("wyreAccId",strWyreAccid);
        ACU.MySP.saveSP(mContext, ACU.MySP.OUTSIDE_VIEW, "");
        ACU.MySP.saveSP(mContext,ACU.MySP.BTNCLICK,"webAppInterface");
        ((Activity) mContext).startActivity(intent);
        ((Activity) mContext).finish();

    }

    @JavascriptInterface
    public void errorData(String error) {
        Log.e("WebAppInterface", "errorData: " + error);
        Intent intent = new Intent(mContext, SellActivity.class);
        ((Activity) mContext).startActivity(intent);
        ((Activity) mContext).finish();
    }



}
