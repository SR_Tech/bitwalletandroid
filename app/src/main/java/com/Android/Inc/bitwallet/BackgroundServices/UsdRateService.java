package com.Android.Inc.bitwallet.BackgroundServices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.util.Log;

import com.Android.Inc.bitwallet.SSl_Classes.AuthenticationParameters;
import com.Android.Inc.bitwallet.SSl_Classes.IOUtil;
import com.Android.Inc.bitwallet.SSl_Classes.SSLContextFactory;
import com.Android.Inc.bitwallet.SSl_Classes.TLSSocketFactory;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.FileUtils;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class UsdRateService extends BroadcastReceiver {

    String result = null;
    String urlParameters = "";
    byte[] postData = null;
    int postDataLength = 0;
    private static SSLContext sslContext = null;
    private static Context context;
    HttpURLConnection urlConnection = null;
    //static String  clientCertificatePassword = "abc@123";
    static String  clientCertificatePassword = "G3edKQab93713!#LH";
    String url = "https://gateway.bitwallet.org/rest/usd_rate";
  //
    private static final String TAG = UsdRateService.class.getSimpleName();


    private static String caCertificateName = "new_crt_cert.crt";
    private static String clientCertificateName = "client_cert.p12";


    @Override
    public void onReceive(final Context context, Intent intent) {
        //public UsdRateService(Context context) {
        this.context = context;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.e(TAG, "onReceive: " + url);
                    URL requestedUrl = new URL(url);
                    urlConnection = (HttpURLConnection) requestedUrl.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setInstanceFollowRedirects(false);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    urlConnection.setRequestProperty("charset", "utf-8");

                    if (urlConnection instanceof HttpsURLConnection) {
                        Log.e(TAG, "getRemoteCall: if condition");

                        if (sslContext == null) {
                            Auth_ssl();
                        }
                        ((HttpsURLConnection) urlConnection)
                                .setSSLSocketFactory(new TLSSocketFactory(sslContext.getSocketFactory(), context));
                    }

                    postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                    postDataLength = postData.length;
                    Log.e("postdataLength", postDataLength + "");
                    urlConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    urlConnection.setUseCaches(false);
                    try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                        wr.write(postData);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    urlConnection.setConnectTimeout(60000);
                    urlConnection.setReadTimeout(60000);
                    result = IOUtil.readFully(urlConnection.getInputStream());
                    JSONObject jsonObject = new JSONObject(result);
                    Log.e(TAG, "onReceive: usd rate :" + jsonObject.toString());
                    String strUsdRate = jsonObject.getString("data");


                    ACU.MySP.saveSP(context, ACU.MySP.USD_RATE, strUsdRate);   //Usd rate
                    Log.e(TAG, "onReceive: usd rate :" + strUsdRate);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }

        });

        thread.start();

    }

//    public void Auth_ssl() {
//        try {
//            AuthenticationParameters authParams = new AuthenticationParameters();
//            authParams.setClientCertificate(IOUtil.getClientCertFile(context));
//            authParams.setClientCertificatePassword(clientCertificatePassword);
//            authParams.setCaCertificate(IOUtil.readCaCert(context));
//            File clientCertFile = authParams.getClientCertificate();
//            sslContext = SSLContextFactory.getInstance().makeContext(clientCertFile,
//                    authParams.getClientCertificatePassword(), authParams.getCaCertificate());
//            CookieHandler.setDefault(new CookieManager());
//        } catch (Exception e) {
//            Log.e("AuthException", e.getMessage());
//        }
//    }


    public static void Auth_ssl() {
        try {
            try {
                AuthenticationParameters authParams = new AuthenticationParameters();
                authParams.setClientCertificate(getClientCertFile());
                authParams.setClientCertificatePassword(clientCertificatePassword);
                authParams.setCaCertificate(readCaCert());

                sslContext = SSLContextFactory.getInstance().makeContext(authParams.getClientCertificate(),
                        authParams.getClientCertificatePassword(),
                        authParams.getCaCertificate());
                CookieHandler.setDefault(new CookieManager());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.e("AuthException", e.getMessage());
        }
    }

    private static File getClientCertFile() throws Exception {
        File certificatefile = FileUtils.fileFromAsset(context, clientCertificateName);
        return certificatefile;
    }

    private static String readCaCert() throws Exception {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(caCertificateName);
        return IOUtil.readFully(inputStream);
    }

}
