package com.Android.Inc.bitwallet.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ACU {

    public static class MySP {

        public static String MAIN_URL = new String("https://gateway.bitwallet.org/rest/");
        // public static String MAIN_URL = new String("http://3.132.74.72:8099/rest/");

        public static final String PREFS_NAME = "AllData";
        public static final String INSTA_URL = "https://www.instagram.com/bitwalletinc/";  // replaced by linked in
        public static String App_BackGnd_ForeGnd_Status = "app_back_fore_status";
        public static String CHECK_APP_STOP = "check_app_stop";
        public static String WEB_VIEW = "web_view";
        public static String OUTSIDE_VIEW = "outside_view";
        public static String BTNCLICK = "btn_click";
        public static String CHARGED_AMT = "charged_amt";
        public static String TIP_RECEIPT_DOLLAR_AMT = "tip_receipt_dollar_amt";
        public static String TIP_RECEIPT_BTC_AMT = "tip_receipt_btc_amt";
        public static String CHECK_FOR_ACTIVITY = "check_for_activity";
        public static String BUSSINESS_USER_TYPE = "business_user_type";
        public static String IS_EMP_SWITCH_ON = "is_emp_switch_on";
        public static String IS_TIP_SWITCH_ON = "is_tip_switch_on";
        public static String IS_IN_BACKGROUND = "is_in_background";
        public static String AUTHORIZATION_TOKEN= "authorization_token";
        public static String PUBLIC_TOKEN= "public_token";
        public static String UPLOAD_DOC_STATUS = "upload_doc_status";
        public static String IS_DEFAULT_WALLET_ID = "is_default_wallet_id";

        public static int RESPONSE_CODE;

        //SelectEmpFromList
        public static String EMP_ID = "emp_id";  // (Emp code)
        public static String EMP_EMAIL = "emp_email";

        //Tip Activity
        public static String TIP_AMT = "tip_amt";
        public static String TIP_PERCENT = "tip_percent";

        //Country code Activity
        public static String COUNTRY_CODE = "country_code";

        // signUp
        public static String REGISTRATION_EMAIL = "registration_email";
        public static String ARROW_VISIBILITY = "arrow_visibility";

        //login credential
        public static String LOGIN_STATUS = "login_status";
        public static String LOGIN_Email= "login_email";
        public static String EMAIL_REMEMBER_ME = "email_remember_me";
        public static String LOGIN_STATUS_BOOLEAN= "login_status_boolean";
        public static String IS_SECURITY_CODE= "is_security_code";


        //logout fragment
        public static String LOGOUT_NO_BTN= "logout_no";
        public static String LOGOUT_YES_BTN= "logout_yes_btn";

        //login
        public static String AUTH_TOKEN = "auth_token";
        public static String USER_TYPE = "user_type";
        public static String BUSINESS_NAME = "business_name";

        //edit wallet fragment
        public static String BTN_CLICK = "btnClick";


        //walletList fragment
        public static String USD_RATE = "usd_rate";
        public static String WALLET_NAME = "wallet_name";
        public static String DOLLER_AMT = "doller_amt";
        public static String BTC_AMT = "btc_amt";
        public static String CURRENCY_TYPE = "currency_type";
        public static String CURRENCY_TYPE_ARRAYLIST = "currency_type_arraylist";
        public static String WALLET_ID = "wallet_id";
        public static String WALLET_BUSINESS_ID = "wallet_business_id";
        public static String WALLET_UNIQUE_ID = "wallet_unique_id";
        public static String WALLET_LIST_FRAGMENT = "wallet_list_fragment";
        public static String WALLET_Type = "wallet_type";

        //Scan Activity
        public static String WALLET_ADDRESS = "wallet_address";

        //SendCurrency fragment
        public static String NETWORK_FEE = "network_fee";
        public static String BTC_AMOUNT = "btc_amount";
        public static String DOLLER_AMOUNT = "doller_amount";
        public static String FEES = "fees";
        public static String RAW_HEX = "raw_hex";
        public static String WALLET_TXN_ID = "wallet_txn_id";

        //Sell Activity
        public static String PAYMENT_MTHD_NAME = "payment_mthd_name";
        public static String PAYMENT_MTHD_ID = "payment_mthd_id";
        public static String GET_SELL_TRANACTION_FEES = "";

        //Sell Activity
        public static String TRASLANG = "tran_amt";

        public static void saveSP(Context mContext, String key, String data) {
            final SharedPreferences SpyAppData = mContext.getSharedPreferences(
                    PREFS_NAME, 0);
            SharedPreferences.Editor editor = SpyAppData.edit();
            editor.putString(key, data);
            editor.commit();
        }

        public static void clearAllPreference(Context context){
            SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
        }
        public static String getFromSP(Context mContext, String key, String dvalu) {
            final SharedPreferences ToolsAppData = mContext.getSharedPreferences(
                    PREFS_NAME, 0);
            final String preData = ToolsAppData.getString(key, dvalu).trim();
            return preData;
        }

        public static void saveTrantion(Context mContext, String key, String data) {
            final SharedPreferences SpyAppData = mContext.getSharedPreferences(
                    TRASLANG, 0);
            SharedPreferences.Editor editor = SpyAppData.edit();
            editor.putString(key, data);
            editor.commit();
        }



        public static String getFromTrantion(Context mContext, String key, String dvalu) {
            final SharedPreferences ToolsAppData = mContext.getSharedPreferences(
                    TRASLANG, 0);
            final String preData = ToolsAppData.getString(key, dvalu).trim();
            return preData;

        }

        public static String getSPString(Context context, String tag, String defltValue) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            return sp.getString(tag, defltValue);
        }

        public static Boolean setSPString(Context context, String tag, String value) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            try {
                sp.edit().putString(tag, value).apply();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        public static Boolean  getSPBoolean(Context context, String key, boolean dvalu) {
            final SharedPreferences ToolsAppData = context.getSharedPreferences(
                    PREFS_NAME, 0);
            final boolean preData = ToolsAppData.getBoolean(key, dvalu);
            return preData;

            /*SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            return sp.getBoolean(tag, defltValue);*/
        }

        public static void setSPBoolean(Context context, String key, boolean data) {
            final SharedPreferences SpyAppData = context.getSharedPreferences(
                    PREFS_NAME, 0);
            SharedPreferences.Editor editor = SpyAppData.edit();
            editor.putBoolean(key, data);
            editor.commit();

           /* SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            try {
                sp.edit().putBoolean(tag, value).apply();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }*/
        }

        public static  void saveArrayList(Context context,String key,List<String> list){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();     // This line is IMPORTANT !!!
        }

        public static List<String> getArrayList(Context context, String key){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<ArrayList<String>>() {}.getType();
            return gson.fromJson(json, type);
        }

        public static  void saveArrayByteList(Context context,String key,ArrayList<byte[]> list){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();     // This line is IMPORTANT !!!
        }

        public static ArrayList<byte[]> getArrayByteList(Context context,String key){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<ArrayList<byte[]>>() {}.getType();
            return gson.fromJson(json, type);
        }

        public static  void saveArraysBytesList(Context context,String key,ArrayList<ArrayList<byte[]>> list){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();     // This line is IMPORTANT !!!
        }

        public static ArrayList<ArrayList<byte[]>> getArraysBytesList(Context context,String key){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<ArrayList<ArrayList<byte[]>>>() {}.getType();
            return gson.fromJson(json, type);
        }


    }
}
