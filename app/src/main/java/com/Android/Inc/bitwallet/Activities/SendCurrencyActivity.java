 package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class SendCurrencyActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {
    private View view, layout;
    private TextView txtScanCode, txtBtcAvailable, txtUseMax, txtAlert, txtnetwkFee;
    private Context context;
    private EditText edtEnterBtcAmt, edtAmtDoller, edtWalletAddress, edtPriority;
    private Button btnSend;
    private ProgressBar progressBar;
    TextWatcher generalTextWatcher = null;
    private int previousLength;
    private String status_code = "";
    private boolean backSpace, typing;
    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    String screenType, priority = "LOW";
    private boolean minimizeBtnPressed = false;
    private View bottom_sheet;
    private List<String> list;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    String str_raw_hex, str_fees = "0", strReceiverAddress, strTxnType = "USEMAX", strSendAmt, strUniqueId,
            strWalletId, strTotalAmt = "0", strCloneWalletId, strCloneWalletStatus, strCloneUniqueId, strIsPaymentWallet;
    private static final String TAG = SendCurrencyActivity.class.getSimpleName();
    public static String getFeesUrl = "crypto-currency/get-txnfee/v1";  // for get fees and use max
    int c = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_send_currency_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_send_currency);
        }

        context = SendCurrencyActivity.this;
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        initialize();

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }


    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        txtScanCode = findViewById(R.id.txt_scanCode);
        txtBtcAvailable = findViewById(R.id.txt_btcAvailable);
        txtUseMax = findViewById(R.id.txt_useMax);
        txtAlert = findViewById(R.id.txt_alert);
        txtnetwkFee = findViewById(R.id.txt_netwkFee);
        edtEnterBtcAmt = findViewById(R.id.edt_enterBtcAmt);
        edtAmtDoller = findViewById(R.id.edt_amtDoller);


        edtWalletAddress = findViewById(R.id.edt_walletAddress);
        btnSend = findViewById(R.id.btn_send);
        progressBar = findViewById(R.id.send_progressBar);
        edtPriority = findViewById(R.id.edt_priority);
        edtPriority.setEnabled(true);

        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);


        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

        txtUseMax.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        txtScanCode.setOnClickListener(this);
        edtPriority.setOnClickListener(this);


        list = new ArrayList<>();
        list.add("LOW");
        list.add("HIGH");

//        Log.e(TAG, "initialize: btc: " + ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, ""));
//        Log.e(TAG, "initialize: USD" + ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
        strTotalAmt = ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "");
        txtBtcAvailable.setText(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "") + " " + ACU.MySP.getFromSP(context, ACU.MySP.CURRENCY_TYPE, "") + " AVAILABLE");


        generalTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                previousLength = s.length();
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void afterTextChanged(Editable s) {
                try {
                    backSpace = previousLength > s.length();
                    if (edtEnterBtcAmt.getText().hashCode() == s.hashCode()) {
                        typing = true;
                        try {
                            edtAmtDoller.setFilters(new InputFilter[]{new InputFilter.LengthFilter(40)});
                            edtAmtDoller.removeTextChangedListener(generalTextWatcher);
                            if (edtEnterBtcAmt.length() == 0) {
                                edtAmtDoller.setText("");

                            } else {

                                final double btcAmt = Double.valueOf((String.valueOf(s)));
                                Log.e("btcAmt", btcAmt + "");
                                Log.e("usd", ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
                                double walletDollerAmt = (double) ((btcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                                Log.e("walletDollerAmt", walletDollerAmt + "");

                                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                Log.e("strWalletDollerAmt", strWalletDollerAmt);

                                edtAmtDoller.setText(strWalletDollerAmt);
                                if (!str_fees.equalsIgnoreCase("0")) {
                                    float sum = Float.sum(Float.valueOf(str_fees), Float.valueOf(edtEnterBtcAmt.getText().toString()));
                                    String strSum = Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf(sum));
                                    strTotalAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf(strTotalAmt));
//                                    Log.e(TAG, "afterTextChanged btc: sum: " + Float.valueOf(strSum));
//                                    Log.e(TAG, "afterTextChanged btc: strTotalAmt: " + Float.valueOf(strTotalAmt));
                                    if (Float.valueOf(strSum).equals(Float.valueOf(strTotalAmt))) { // if fee and snd amt is eql to total amt means usemax
                                        strTxnType = "USEMAX";
                                    } else {
                                        strTxnType = "";  // if not usemax
                                    }
                                }
//                                ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, strWalletDollerAmt);
                                ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, strWalletDollerAmt);
                            }
                            edtAmtDoller.addTextChangedListener(generalTextWatcher);
                        } catch (Exception e) {
                            e.printStackTrace();
                            edtAmtDoller.addTextChangedListener(generalTextWatcher);
                            Log.e("exception btc", e.getMessage());
                            // CustomToast.custom_Toast(contextDashBoard, "incorrect input", layout);
                            //   CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
                        }
                    } else if (edtAmtDoller.getText().hashCode() == s.hashCode()) {
                        edtAmtDoller.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        if (typing) {
                            if (backSpace) {
                                edtAmtDoller.setText("");
                                edtEnterBtcAmt.setText("");
                            }
                            typing = false;
                        }
                        edtEnterBtcAmt.removeTextChangedListener(generalTextWatcher);

                        if (edtAmtDoller.length() == 0) {
                            edtEnterBtcAmt.setText("");
                        } else {
                            final double dollerAmt = Double.valueOf((String.valueOf(s)));
                            final float btcAmt = (float) (dollerAmt / Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));
                            DecimalFormat numberFormat = new DecimalFormat("#0.00000000");
                            final String strBtcAmt = numberFormat.format(btcAmt);
                            edtEnterBtcAmt.setText(strBtcAmt);
                            if (!str_fees.equalsIgnoreCase("0")) {
                                float sum = Float.sum(Float.valueOf(str_fees), Float.valueOf(edtEnterBtcAmt.getText().toString()));
                                String strSum = Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf(sum));
                                strTotalAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf(strTotalAmt));
                                Log.e(TAG, "afterTextChanged dollar: sum: " + Float.valueOf(strSum));
                                //strTotalAmt = Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(strTotalAmt));
                                //Log.e(TAG, "afterTextChanged: strTotalAmt : " + strTotalAmt);
                                Log.e(TAG, "afterTextChanged dollar: strTotalAmt : " + Float.valueOf(strTotalAmt));
                                if (Float.valueOf(strSum).equals(Float.valueOf(strTotalAmt))) { // if fee and snd amt is eql to total amt means usemax
                                    strTxnType = "USEMAX";
                                } else {
                                    strTxnType = "";  // if not usemax
                                }
                            }
                        }

                        edtEnterBtcAmt.addTextChangedListener(generalTextWatcher);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    edtEnterBtcAmt.addTextChangedListener(generalTextWatcher);
                    Log.e("exception dollar", e.getMessage());
                    //dialogSendCurrencyActivity("Please Provide Valid Input");
                }
            }


        };


        edtAmtDoller.addTextChangedListener(generalTextWatcher);
        edtEnterBtcAmt.addTextChangedListener(generalTextWatcher);

        ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "");
        ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, "");
        ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, "");

        edtAmtDoller.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                c = 0;
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {

                    edtPriority.setText("");
                    priority = "";
                    strTxnType = "";

                } else {
                    if (c == 0) {
                        Log.d(TAG, "onTouch: wwwkk");
                        CustomDialogs.dialogShowMsg(context, screenType,  getString(R.string.zerobalence));
                        edtAmtDoller.setEnabled(false);
                    }
                    c++;
                }
                return false;
            }
        });

        edtEnterBtcAmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                c = 0;
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {

                    edtPriority.setText("");
                    priority = "";
                    strTxnType = "";

                } else {
                    if (c == 0) {
                        Log.d(TAG, "onTouch: wwwkk");
                        CustomDialogs.dialogShowMsg(context, screenType,  getString(R.string.zerobalence));
                        edtEnterBtcAmt.setEnabled(false);
                    }
                    c++;
                }
                return false;
            }
        });

    }

    public boolean Validate() {
        if (VU.isEmpty(edtEnterBtcAmt)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_BTC_Amount));
            return false;
        } else if (VU.isEmpty(edtAmtDoller)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Amount_In_Doller));
            return false;
        } else if (VU.isEmpty(edtWalletAddress)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Wallet_Address_To_Send_BTC_Amount));
            return false;
        } else if (edtAmtDoller.getText().toString().equalsIgnoreCase("") ||
                edtAmtDoller.getText().toString().equalsIgnoreCase("0") ||
                edtAmtDoller.getText().toString().equalsIgnoreCase("00") ||
                edtAmtDoller.getText().toString().equalsIgnoreCase("000") ||
                edtAmtDoller.getText().toString().equalsIgnoreCase("0000") ||
                edtAmtDoller.getText().toString().equalsIgnoreCase("00000") ||
                edtAmtDoller.getText().toString().equalsIgnoreCase("000000")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Amount_In_Doller));
            return false;
        } else if (edtAmtDoller.getText().toString().equalsIgnoreCase("0.")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (edtAmtDoller.getText().toString().equalsIgnoreCase(".0")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (edtEnterBtcAmt.getText().toString().equalsIgnoreCase("") ||
                edtEnterBtcAmt.getText().toString().equalsIgnoreCase("0") ||
                edtEnterBtcAmt.getText().toString().equalsIgnoreCase("00") ||
                edtEnterBtcAmt.getText().toString().equalsIgnoreCase("000") ||
                edtEnterBtcAmt.getText().toString().equalsIgnoreCase("0000") ||
                edtEnterBtcAmt.getText().toString().equalsIgnoreCase("00000") ||
                edtEnterBtcAmt.getText().toString().equalsIgnoreCase("000000")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_BTC_Amount));
            return false;
        } else if (edtEnterBtcAmt.getText().toString().equalsIgnoreCase("0.")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (edtEnterBtcAmt.getText().toString().equalsIgnoreCase(".0")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } /*else if (edtPriority.getText().toString().equals("")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_priority));
            return false;
        }*/
        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_useMax:
                c = 0;
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {
                    txtAlert.setVisibility(View.INVISIBLE);
                    ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "");
                    if (VU.isConnectingToInternet(context, screenType)) {
//                    strTxnType = "USEMAX";
                        if (edtWalletAddress.getText().toString().trim().equals(""))

                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Wallet_Address_To_Send_BTC_Amount));
                        else if (edtPriority.getText().toString().equals(""))
                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_priority));
                        else {
                            strTxnType = "USEMAX";
                            USEMAX_API();
                        }
                    }
                }else {
                    if (c == 0) {
                        CustomDialogs.dialogShowMsg(context, screenType,  getString(R.string.zerobalence));
                        edtEnterBtcAmt.setEnabled(false);
                    }
                    c++;
                }

                break;

            case R.id.txt_scanCode:

                c = 0;
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {

                    txtAlert.setVisibility(View.INVISIBLE);
                    ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                    ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "");
                    edtAmtDoller.setText("");
                    edtEnterBtcAmt.setText("");
                    edtWalletAddress.setText("");
                    edtPriority.setText("");
                    txtnetwkFee.setVisibility(View.GONE);
                    Intent intent = new Intent(context, ScanCodeActivity.class);
                    startActivityForResult(intent, 2);// Activity is started with requestCode 2
                    //  finish();

                }else {
                    if (c == 0) {

                        CustomDialogs.dialogShowMsg(context, screenType,  getString(R.string.zerobalence));
                        edtEnterBtcAmt.setEnabled(false);
                    }
                    c++;
                }
                break;

            case R.id.btn_send:
                txtAlert.setVisibility(View.INVISIBLE);
                ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "");
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        if (edtPriority.getText().toString().equals("")) {
                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_priority));
                        } else if (!str_fees.equalsIgnoreCase("0")) {
                            Log.e(TAG, "onClick: strTxnType: " + strTxnType);
                            if ((Double.valueOf(edtEnterBtcAmt.getText().toString()) > (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, ""))))) {
                                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.not_enough_btc));
                            } else if (edtEnterBtcAmt.getText().toString().equals(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, ""))) {  // when sending whole wallet amt
                                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.useMax_use));   //use useMax functionality msg
                            } else if (strTxnType.equalsIgnoreCase("USEMAX")) {
                                ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, edtWalletAddress.getText().toString().trim());
                                gotoSendingCurrencyActivity();
                            } else {
                                ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, edtWalletAddress.getText().toString().trim());
                                gotoSendingCurrencyActivity();
                            }
                        }
                    }
                }
                break;

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
            case R.id.edt_priority:
                try {
                    if (VU.isConnectingToInternet(context, screenType)) {

                        if (strTxnType.equalsIgnoreCase("USEMAX")) {
                            showBottomSheetDialog();
                        } else if (edtAmtDoller.getText().toString().equals("") &&
                                edtEnterBtcAmt.getText().toString().equals("")) {
                            strTxnType="USEMAX";
                            showBottomSheetDialog();
                            /*float sum = Float.valueOf(edtEnterBtcAmt.getText().toString());
                            String strSum = Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf(sum));
                            Log.e(TAG, "onClick: " + strSum);
                            Log.e(TAG, "onClick: " + strTotalAmt);*/
                            //  strTotalAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf(strTotalAmt));
                           /* if (Float.valueOf(strSum).equals(Float.valueOf(strTotalAmt))) { // if fee and snd amt is eql to total amt means usemax
                                strTxnType = "USEMAX";
                            } else {*/
                            //  strTxnType = "";  // if not usemax
                            // }

                        } else {
                            if (Validate())
                                showBottomSheetDialog();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    //scan Activity result
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            if (data != null)
                strTxnType = data.getStringExtra("txnType");
        }
    }

    @Override
    protected void onResume() {

        try {
            if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
                startActivity(new Intent(context, SplashActivity.class));
                finish();
            }

            String str_barCode = ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ADDRESS, "");
            String str_btcAmt = ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMOUNT, "");

            if (str_barCode.length() > 36 || str_barCode.length() < 26) {
                if (str_barCode.length() == 0) {
                    txtAlert.setVisibility(View.INVISIBLE);
                } else if (str_barCode.equalsIgnoreCase("invalid address")) {
                    txtAlert.setVisibility(View.VISIBLE);
                    edtWalletAddress.setText("");
                    edtEnterBtcAmt.setText("");
                }

            } else {
                txtAlert.setVisibility(View.INVISIBLE);
                edtWalletAddress.setText(str_barCode);
                edtEnterBtcAmt.setText(str_btcAmt);

            }
        } catch (Exception e) {
            Log.e("errorSend", e.getMessage());
        }
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, TransactionActivity.class);
        startActivity(intent);
        finish();
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    private void USEMAX_API() {
        Log.e(TAG, "USEMAX_API: strTxnType: " + strTxnType);
        progressBar.setVisibility(View.VISIBLE);
        //  edtPriority.setText(priority);
        String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "")
                + "&txnType=" + strTxnType + "&wallettype=MAIN" + "&username=" + "&paymentId=" + "&wyreAccId="
                + "&send_to=" + edtWalletAddress.getText().toString().trim() + "&amount=" + ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")
                + "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes())
                + "&priority=" + priority;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(getFeesUrl);
        helper.setUrlParameter(urlParameters);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);

                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        // String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("message");
                        Log.e(TAG, "onPostExecute: " + msg);
                        status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status && status_code.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (dataObject != null) {
                                str_raw_hex = dataObject.getString("singrawhexastr");
                                str_fees = Miscellaneous.CustomsMethods.convertToBTCFormat(dataObject.getString("txnfee"));
                                Log.e(TAG, "processResponse: str_fees: " + str_fees);
                                strReceiverAddress = dataObject.getString("receiverAddr");
                                strTxnType = dataObject.getString("txnType");
                                strSendAmt = dataObject.getString("send_amt");
                                strUniqueId = dataObject.getString("uniqueId");
                                strWalletId = dataObject.getString("wallet_id");
                                strTotalAmt = dataObject.getString("total_amt");
                                strCloneWalletStatus = dataObject.getString("clone_wallet_status");
                                strCloneWalletId = dataObject.getString("clone_wallet_id");
                                strCloneUniqueId = dataObject.getString("clone_uniqueId");
                                strIsPaymentWallet = dataObject.getString("is_payment_wallet");
                                Log.e(TAG, "onPostExecute: strTxnType: " + strTxnType);
                                Log.e(TAG, "onPostExecute: str_fees: " + str_fees);
                                Log.e(TAG, "onPostExecute: strReceiverAddress: " + strReceiverAddress);
                                Log.e(TAG, "onPostExecute: strSendAmt: " + strSendAmt);
                                Log.e(TAG, "onPostExecute: strTotalAmt: " + strTotalAmt);
                                Log.e(TAG, "onPostExecute: strUniqueId: " + strUniqueId);
                                ACU.MySP.saveSP(context, ACU.MySP.NETWORK_FEE, str_fees);
                                edtAmtDoller.setText(Miscellaneous.CustomsMethods.convertBtcToDoller(strSendAmt, context));
                                edtEnterBtcAmt.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(strSendAmt));
                                txtnetwkFee.setVisibility(View.VISIBLE);
                                txtnetwkFee.setText("NETWORK FEES " + str_fees + " BTC");
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        } else {
                            str_fees = "0";
                            txtnetwkFee.setVisibility(View.GONE);
                            priority = "LOW";
                            Dialog d = CustomDialogs.dialogShowMsg(context, screenType, msg);
                            d.findViewById(R.id.btn_ok)
                                    .setOnClickListener(v -> {
                                        edtPriority.setText("");
                                        d.dismiss();
                                    });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        });
        restAsyncTask.execute();

    }


    private void sendCurrencyAPI() {
        ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, edtEnterBtcAmt.getText().toString());
        ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, edtAmtDoller.getText().toString());
        progressBar.setVisibility(View.VISIBLE);
        String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "")
                + "&txnType=SEND" + "&wallettype=MAIN" + "&username=" + "&paymentId=" + "&wyreAccId="
                + "&send_to=" + edtWalletAddress.getText().toString().trim() + "&amount=" + edtEnterBtcAmt.getText().toString()
                + "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes())
                + "&priority=" + priority;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(getFeesUrl);
        helper.setUrlParameter(urlParameters);

        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status && status_code.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (dataObject != null) {
                                str_raw_hex = dataObject.getString("singrawhexastr");
                                str_fees = Miscellaneous.CustomsMethods.convertToBTCFormat(dataObject.getString("txnfee"));

                                strSendAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(dataObject.getString("send_amt"));

//                              //vinay
//                                strSendAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(dataObject.getString("send_amt"));

                                strTotalAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(dataObject.getString("total_amt"));
                                strReceiverAddress = dataObject.getString("receiverAddr");
                                strTxnType = dataObject.getString("txnType");
                                strUniqueId = dataObject.getString("uniqueId");
                                strWalletId = dataObject.getString("wallet_id");
                                strCloneWalletStatus = dataObject.getString("clone_wallet_status");
                                strCloneWalletId = dataObject.getString("clone_wallet_id");
                                strCloneUniqueId = dataObject.getString("clone_uniqueId");
                                strIsPaymentWallet = dataObject.getString("is_payment_wallet");
                                Log.e(TAG, "onPostExecute: strTxnType: " + strTxnType);
                                Log.e(TAG, "onPostExecute: str_fees: " + str_fees);
                                Log.e(TAG, "onPostExecute: strReceiverAddress: " + strReceiverAddress);
                                Log.e(TAG, "onPostExecute: strSendAmt: " + strSendAmt);
                                Log.e(TAG, "onPostExecute: strTotalAmt: " + strTotalAmt);
                                Log.e(TAG, "onPostExecute: strUniqueId: " + strUniqueId);
                                //    String str_is_otp = dataObject.getString("is_opt");
                                ACU.MySP.saveSP(context, ACU.MySP.NETWORK_FEE, str_fees);
                                txtnetwkFee.setVisibility(View.VISIBLE);
                                txtnetwkFee.setText("NETWORK FEES " + str_fees + " BTC");

                            /*if (str_is_otp.equalsIgnoreCase("true")) {
                                startActivity(new Intent(context, OTPVerifyForPaymentActivity.class));

                            } else {*/
                                ACU.MySP.saveSP(context, ACU.MySP.FEES, str_fees);
                                ACU.MySP.saveSP(context, ACU.MySP.RAW_HEX, str_raw_hex);
                                //  }
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        } else {
                            priority = "LOW";
                            str_fees = "0";
                            txtnetwkFee.setVisibility(View.GONE);
                            Dialog d = CustomDialogs.dialogShowMsg(context, screenType, msg);
                            d.findViewById(R.id.btn_ok)
                                    .setOnClickListener(v -> {
                                        edtPriority.setText("");
                                        d.dismiss();
                                    });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        });
        restAsyncTask.execute();
    }

    private void gotoSendingCurrencyActivity() {
        Intent intent = new Intent(context, SendingCurrencyActivity.class);
        intent.putExtra("address", strReceiverAddress);
        intent.putExtra("sendAmt", strSendAmt);
        intent.putExtra("txnHash", str_raw_hex);
        intent.putExtra("uniqueId", strUniqueId);
        intent.putExtra("txnfee", str_fees);
        intent.putExtra("txnType", strTxnType);
        intent.putExtra("totalAmt", strTotalAmt);
        intent.putExtra("walletId", strWalletId);
        intent.putExtra("cloneUniqueId", strCloneUniqueId);
        intent.putExtra("cloneWalletId", strCloneWalletId);
        intent.putExtra("cloneWalletStatus", strCloneWalletStatus);
        intent.putExtra("isPaymentWallet", strIsPaymentWallet);
        ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "send_currency_activity");
        startActivity(intent);
        finish();
    }

    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.wallet_bottom_list, null);
        final ListView currencyTypeList = view.findViewById(R.id.list_bottom);

        Log.e("List", list.toString());

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.adapter_wallet_bottom_list, R.id.textView, list);
        currencyTypeList.setAdapter(arrayAdapter);

        currencyTypeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                edtPriority.setText(list.get(position));
                priority = list.get(position);
             /*   if (strTxnType.equalsIgnoreCase("USEMAX") && !edtAmtDoller.getText().toString().equalsIgnoreCase("")) {
                    edtEnterBtcAmt.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(String.valueOf((Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, ""))) - (Double.valueOf(str_fees)))));
                }*/
                Log.e(TAG, "onItemClick: strTxnType: " + strTxnType);

                if (strTxnType.equalsIgnoreCase("") || strTxnType.equalsIgnoreCase("SEND"))
                    sendCurrencyAPI();
                else {
                    if ((!str_fees.equalsIgnoreCase("0")))
                        USEMAX_API();
                }

                Log.e(TAG, "onItemClick: " + priority);
                mBottomSheetDialog.cancel();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

}
