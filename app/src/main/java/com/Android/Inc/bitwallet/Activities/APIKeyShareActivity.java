package com.Android.Inc.bitwallet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

public class APIKeyShareActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener{

    private static final String TAG = APIKeyShareActivity.class.getSimpleName();
    private ProgressBar progressBar;
    private Context context;
    EditText edtkey;
    String api_key;
    String screenType;
    Button btn_generate_key,btn_view_key,btn_copy_key,btn_cancel_key;



    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    public static String API_CREATE_KEY = "plugin/create";
    public static String API_GET_KEY = "plugin/get-apikey";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_apikey_share_tab);

        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_apikey_share);
        }

        initialize();


        context = APIKeyShareActivity.this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        edtkey = findViewById(R.id.edt_apikey);




    }


    private void initialize() {

        ll_backArrow = findViewById(R.id.back_layout);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        btn_generate_key = findViewById(R.id.btn_generate_key);
        btn_view_key = findViewById(R.id.btn_view_key);
        btn_copy_key = findViewById(R.id.btn_copy_key);
        btn_cancel_key = findViewById(R.id.btn_cancel_key);
        progressBar = findViewById(R.id.account_progressBar);

        btn_copy_key.setVisibility(View.GONE);

        btn_generate_key.setOnClickListener(this);
        btn_view_key.setOnClickListener(this);
        btn_copy_key.setOnClickListener(this);
        btn_cancel_key.setOnClickListener(this);


        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SettingActivity.class));
        finish();
        super.onBackPressed();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", "https://www.instagram.com/bitwalletinc/");
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.back_layout:
                onBackPressed();
                break;

            case R.id.btn_generate_key:
                if (VU.isConnectingToInternet(context, screenType)) {
                        generateKey();
                }
                break;

            case R.id.btn_view_key:
                if (VU.isConnectingToInternet(context, screenType)) {
                    getKey();
                }
                break;

            case R.id.btn_copy_key:

                setClipboard(context, api_key);

                break;

            case R.id.btn_cancel_key:
                onBackPressed();
                break;

        }

    }

    private void setClipboard(Context context, String text) {
        View layout = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
            CustomToast.custom_Toast_CopyKey(context, "Key copied", layout);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
            CustomToast.custom_Toast_CopyKey(context, "Key copied", layout);


        }
    }

    private void generateKey() {
        String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") ;
        Log.e(TAG, "wallet_id: "+urlParameters );
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(API_CREATE_KEY);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);

                Log.e(TAG, "processResponse: "+response );
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status && status_code.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (dataObject != null) {
//                                String str_raw_hex = dataObject.getString("singrawhexastr");
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }
    private void getKey() {
        //String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") ;
       // Log.e(TAG, "wallet_id: "+urlParameters );
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(API_GET_KEY);
        helper.setUrlParameter("");
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);

                Log.e(TAG, "processResponse: "+response );
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status && status_code.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (dataObject != null) {
                                api_key = dataObject.getString("api_key");
                                edtkey.setText(api_key);
                                btn_copy_key.setVisibility(View.VISIBLE);

                                btn_copy_key.setEnabled(true);
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }
    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context,screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

}