package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.Android.Inc.bitwallet.Activities.PinActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;

public class SelectDocumentForUpload extends AppCompatActivity implements View.OnClickListener , LifecycleObserver {

    private Context context;
    private String screenType, strWyreAccId;
    private LinearLayout ll_backArrow;
    private boolean minimizeBtnPressed = false;
    private RelativeLayout rlPassport, rlDriverLicense, rlIdentityCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = SelectDocumentForUpload.this;
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_select_document_for_upload_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_select_document_for_upload);
        }
        init();
        getIntentData();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    private void init() {
        rlPassport = findViewById(R.id.rl_passport);
        rlDriverLicense = findViewById(R.id.rl_driver_license);
        rlIdentityCard = findViewById(R.id.rl_identity_card);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);

        rlPassport.setOnClickListener(this);
        rlDriverLicense.setOnClickListener(this);
        rlIdentityCard.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strWyreAccId = bundle.getString("wyreAccId");
            String strDocumentType = bundle.getString("documentStatusType");
            ACU.MySP.saveSP(context,ACU.MySP.UPLOAD_DOC_STATUS,strDocumentType);

        }

        if (ACU.MySP.getFromSP(context,ACU.MySP.UPLOAD_DOC_STATUS," ").equals("0")){   // gov id
            rlIdentityCard.setVisibility(View.GONE);
        }else {    // address proof
            rlPassport.setVisibility(View.GONE);
            rlDriverLicense.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
            case R.id.rl_passport:
                gotoUploadDocActivity("passport");
                break;
            case R.id.rl_driver_license:
                gotoUploadDocActivity("driver_license");
                break;
            case R.id.rl_identity_card:
                gotoUploadDocActivity("identity_card");
                break;
        }
    }

    private void gotoUploadDocActivity(String type) {
        Intent intent = new Intent(context, UploadPhotoActivity.class);
        intent.putExtra("docType", type);
        intent.putExtra("wyreAccId", strWyreAccId);
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, SellActivity.class));
        finish();
    }
}
