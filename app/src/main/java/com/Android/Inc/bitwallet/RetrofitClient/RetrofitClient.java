package com.Android.Inc.bitwallet.RetrofitClient;


import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitClient {
    //public static final String Base_URL="https://apiv2.bitcoinaverage.com/indices/global/history/";
    public static final String Base_URL="https://gateway.bitwallet.org/rest/";
//    public static final String Base_URL = "http://3.13.233.35:8099/rest/";
//    public static final String Base_URL = "http://3.13.233.35:8099/rest/";
  //  public static String DEV_URK = new String("http://3.13.233.35:8099/rest/");

    public static RetrofitClient mInstance;
    public static Retrofit retrofit = null;
    private RetrofitClient() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(100, TimeUnit.SECONDS)
                .connectTimeout(100, TimeUnit.SECONDS)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Base_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }
    public Api getApi() {
        return retrofit.create(Api.class);
    }
}

