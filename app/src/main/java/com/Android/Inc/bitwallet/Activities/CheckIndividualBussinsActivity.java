package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;

public class CheckIndividualBussinsActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnIndividual, btnBusiness;
    private Context context;
    private LinearLayout ll_drawer;
    String screenType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_check_individual_business_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_check_indivisual_bussins);
        }
        context = CheckIndividualBussinsActivity.this;

        initialize();
    }

    private void initialize() {
        btnBusiness = findViewById(R.id.btn_business);
        btnIndividual = findViewById(R.id.btn_indivisual);
        ll_drawer = findViewById(R.id.title_bar_left_menu);

        btnBusiness.setOnClickListener(this);
        btnIndividual.setOnClickListener(this);
        ll_drawer.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_business:
                startActivity(new Intent(context, SignUpBusinessActivity.class));
                finish();
                break;

            case R.id.btn_indivisual:
                startActivity(new Intent(context, SignUpIndividualActivity.class));
                finish();
                break;

            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("login")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, LoginActivity.class));
            finish();
        } else {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        }
        super.onBackPressed();
    }
}