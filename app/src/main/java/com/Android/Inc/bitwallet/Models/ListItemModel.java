package com.Android.Inc.bitwallet.Models;

public class ListItemModel {

    private String Name;
    private String Code;
    private String shortName;


    public ListItemModel(String name) {
        Name = name;
    }

    public ListItemModel(){}
    public ListItemModel(String Name, String Code) {
        this.Name = Name;
        this.Code = Code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
