package com.Android.Inc.bitwallet;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.icu.util.GregorianCalendar;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.Activities.MainActivity;
import com.Android.Inc.bitwallet.BackgroundServices.UsdRateService;


public class SplashActivity extends AppCompatActivity {
    Context context;
    private static int SPLASH_TIME_OUT = 2000;
    private PendingIntent pendingIntent;
    private AlarmManager manager;
    private int interval = 500;
    long repeatInterval = 1000 * 3;
    long triggerTime = 1000
            ;
//+ repeatInterval
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_splash);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_splash_tab);
        } else {
            setContentView(R.layout.activity_splash);
        }

        context = SplashActivity.this;
        UsdRateServiceCall();

    }

    private void UsdRateServiceCall() {
        Intent alarmIntent = new Intent(this, UsdRateService.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= 19) {
            manager.setRepeating(AlarmManager.RTC_WAKEUP,
                    triggerTime,
                    interval,
                    pendingIntent);
        } else {
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    triggerTime,
                    interval,
                    pendingIntent);
        }

//        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time, interval , pendingIntent);
        //  new UsdRateService(context);

        splashTime();

    }

    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                //   startActivity(new Intent(SplashActivity.this, FilterNEmployeesActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
