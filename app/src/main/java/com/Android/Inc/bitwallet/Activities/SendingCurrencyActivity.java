package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.PaymentTokenActivity;
import com.Android.Inc.bitwallet.Activities.BuySellActivity.SellActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;

public class SendingCurrencyActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow,llWyreFees,llTotalAmt;
    private View layout;
    private Context context;
    private TextView txtTotalBtcSend, txtTotalDollersend, txtNetworkFees, txtWalletAddress,txtWyreFee,txtFinalRecAmt,txtHeading;
    private TextView btn_confirm;
    private ProgressBar progressBar;
    private String status_code = "", strBackType = "", strWyreAccId;
    String screenType;
    private boolean minimizeBtnPressed = false;
    private Bundle bundle = null;
    private static final String TAG = SendingCurrencyActivity.class.getSimpleName();
    public static String sendTxtUrl = "crypto-currency/send-txn";
    private String strSndAmt, strTxnFee, strAddress, strTxnType, strTotalAmt, strWalletId, strTxnHash, strUniqueId,strSndDollarAmt,
            strCloneWalletId,strCloneWalletStatus,strCloneUniqueId,strWyreFinalRecAmt,strWyreFee,strWyreFeeRate,strIsPaymentWallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_sending_currency_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_sending_currency);
        }

        context = SendingCurrencyActivity.this;
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        getIntentData();
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    private void getIntentData() {
        bundle = getIntent().getExtras();
        strBackType =  ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "");
        Log.e(TAG, "getIntentData: strBackType: "+strBackType );
        if (bundle != null) {
            strTxnFee = bundle.getString("txnfee");
            strSndAmt = bundle.getString("sendAmt");
            strAddress = bundle.getString("address");
            strTxnType = bundle.getString("txnType");
            strTotalAmt = bundle.getString("totalAmt");
            strWalletId = bundle.getString("walletId");
            strTxnHash = bundle.getString("txnHash");
            strUniqueId = bundle.getString("uniqueId");
            strCloneWalletId = bundle.getString("cloneWalletId");
            strCloneWalletStatus = bundle.getString("cloneWalletStatus");
            strCloneUniqueId = bundle.getString("cloneUniqueId");
            strIsPaymentWallet = bundle.getString("isPaymentWallet");
            strSndDollarAmt  = Miscellaneous.CustomsMethods.convertBtcToDoller(strSndAmt,context);
            Log.e(TAG, "getIntentData: strSndDollarAmt: "+strSndDollarAmt );
            ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(strSndAmt)));
            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, strAddress);
            Log.e(TAG, "getIntentData: " + strTxnFee);
            ACU.MySP.saveSP(context, ACU.MySP.NETWORK_FEE, Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(strTxnFee)));
            String btc = ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMOUNT, "");
            Log.e(TAG, "getIntentData: " + btc);
            ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, Miscellaneous.CustomsMethods.convertBtcToDoller(btc, context));
            Log.e(TAG, "getIntentData:strTxnFee: " + strTxnFee + " strSndAmt: " + strSndAmt + " strAddress: " + strAddress + " strTxnType: " + strTxnType +
                    " strTotalAmt: " + strSndAmt + " strWalletId: " + strWalletId + " strTxnHash" + strTxnHash + " strUniqueId " + strUniqueId);

            if (strBackType.equalsIgnoreCase("sell_activity")){
                strWyreFinalRecAmt = bundle.getString("wyreFinalRecAmt");
                strWyreFee = bundle.getString("wyreFee");
                strWyreFeeRate = bundle.getString("wyreFeeRate");
            }
        }

    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {

        txtTotalBtcSend = findViewById(R.id.txt_totalBtcAmtSend);
        txtTotalDollersend = findViewById(R.id.txt_totalDollarAmtSend);
        txtNetworkFees = findViewById(R.id.txt_network_fees);
        txtWalletAddress = findViewById(R.id.txt_walletAddress);
        btn_confirm = findViewById(R.id.btn_confirm);
        progressBar = findViewById(R.id.sending_progressBar);
        txtWyreFee = findViewById(R.id.txt_wyre_fee);
        txtFinalRecAmt = findViewById(R.id.txt_final_Rec_amt);
        llWyreFees = findViewById(R.id.ll_wyre_fee);
        llTotalAmt = findViewById(R.id.ll_final_Rec_amt);
        txtHeading = findViewById(R.id.txt_fragmentName);

        Log.e("NetwkFee", ACU.MySP.getFromSP(context, ACU.MySP.NETWORK_FEE, ""));
        Log.e("Address", ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ADDRESS, ""));

        if (strBackType.equalsIgnoreCase("sell_activity")) {
            txtTotalBtcSend.setText(strSndAmt + " " +
                    ACU.MySP.getFromSP(context, ACU.MySP.CURRENCY_TYPE, ""));
//            Log.e(TAG, "initialize: dollae : amt"+Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, "")) );
            txtTotalDollersend.setText("$ " +(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, "")));
////vinay
//            txtTotalDollersend.setText("$ " + Miscellaneous.CustomsMethods.convertToUSDFormat((ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, ""))));

            txtNetworkFees.setText("NETWORK FEE OF " + (strTxnFee));
            txtHeading.setText("SELL NOW");
            txtWalletAddress.setText(strAddress);
            txtFinalRecAmt.setVisibility(View.VISIBLE);
            txtWyreFee.setText("$ " +Miscellaneous.CustomsMethods.convertToUSDFormat(String.valueOf(Double.valueOf(strWyreFee))));
            txtFinalRecAmt.setText("$ " +Miscellaneous.CustomsMethods.convertToUSDFormat(
                    String.valueOf(Double.valueOf(strSndDollarAmt) -  Double.valueOf(strWyreFee))));

        } else {
            llWyreFees.setVisibility(View.GONE);
            llTotalAmt.setVisibility(View.GONE);
            String strDollarAmt = ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, "");
            txtTotalBtcSend.setText(strSndAmt + " " + ACU.MySP.getFromSP(context, ACU.MySP.CURRENCY_TYPE, ""));

//            Log.e(TAG, "initialize: dollae : amt"+Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, "")) );

            txtTotalDollersend.setText("$ " + strDollarAmt);


            //vinay
//            txtTotalDollersend.setText("$ " + Miscellaneous.CustomsMethods.convertToUSDFormat((strDollarAmt)));

            txtNetworkFees.setText("NETWORK FEE OF " + strTxnFee);
            txtHeading.setText("SENDING");
            txtWalletAddress.setText(strAddress);
        }

        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                if (VU.isConnectingToInternet(context, screenType)) {
                    sendTransaction();
                }
                break;
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public void paymentSuccessConfirmDialog(String type) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_payment_success_tab);
        } else {
            dialog.setContentView(R.layout.dialog_payment_success);
        }
        dialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        TextView txtSentAmt = dialog.findViewById(R.id.txt_sentAmt);
        txtSentAmt.setText("Total " + strSndAmt + " BTC " + type + " Successfully");


        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(context, PaymentSuccessActivity.class);
                intent.putExtra("type",strBackType);
                intent.putExtra("wyreFinalRecAmt",strWyreFinalRecAmt);
                startActivity(intent);
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void sendTransaction() {
        String urlParameters = "wallet_id=" + strWalletId + "&receiverAddr=" + strAddress + "&txnType=" + strTxnType +
                "&total_amt=" + strTotalAmt + "&bitpayfee=" + "&send_amt=" + strSndAmt + "&txnfee=" + strTxnFee +
                "&txnhexastr=" + strTxnHash + "&bitpayaddr=" + "&uniqueId=" + strUniqueId+
                "&clone_wallet_id=" + strCloneWalletId+ "&clone_wallet_status=" + strCloneWalletStatus+ "&clone_uniqueId=" + strCloneUniqueId+
                "&is_payment_wallet="+strIsPaymentWallet;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(sendTxtUrl);
        helper.setUrlParameter(urlParameters);

        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equals("200") && status) {
                            paymentSuccessConfirmDialog("sent");
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();
    }

    public void dialogShowCustomMsg(String msg) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(msg);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(context, TransactionActivity.class));
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
        Log.e(TAG, "onBackPressed: strBackType: " + strBackType);
        if (strBackType.equalsIgnoreCase("send_currency_activity")) {
            startActivity(new Intent(context, SendCurrencyActivity.class));
            finish();
        } else if (strBackType.equalsIgnoreCase("sell_activity")) {
            startActivity(new Intent(context, SellActivity.class));
            finish();
        }
    }
}
