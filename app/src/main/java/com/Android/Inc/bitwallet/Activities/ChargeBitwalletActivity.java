package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.concurrent.ScheduledExecutorService;

public class ChargeBitwalletActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver {

    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, btnDot, btnback;
    EditText edtTotalDollerValue;
    TextView txtBtcAmt;
    TextWatcher generalTextWatcher = null;
    private View layout;
    Context context;
    Button btnCharge;
    ImageView imgWallet;
    LinearLayout ll_backArrow;
    private boolean minimizeBtnPressed = false;
    ScheduledExecutorService scheduleTaskExecutor = null;
    private static int SPLASH_TIME_OUT = 120;
    private ProgressBar progressBar;
    private RestAsyncTask restAsyncTask = null;
    private static final String getWalletIdUrl = "business/v2/get-payment-walletId";

    public static String walletUrl = "business/v2/get_walletList";

    private static final String TAG = ChargeBitwalletActivity.class.getSimpleName();
    String screenType;
    int c = 0;

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_charge_bitwallet_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_charge_bitwallet);
        }

        context = ChargeBitwalletActivity.this;

        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

        initialize();
        if (isConnectingToInternet(context, screenType)) {
            getWalletId();   //wallet id for bussiness Side
        }

        convertDollarToBtc();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        b0 = findViewById(R.id.btn_0);
        b1 = findViewById(R.id.btn_1);
        b2 = findViewById(R.id.btn_2);
        b3 = findViewById(R.id.btn_3);
        b4 = findViewById(R.id.btn_4);
        b5 = findViewById(R.id.btn_5);
        b6 = findViewById(R.id.btn_6);
        b7 = findViewById(R.id.btn_7);
        b8 = findViewById(R.id.btn_8);
        b9 = findViewById(R.id.btn_9);

        btnback = findViewById(R.id.btn_back);
        btnDot = findViewById(R.id.btn_dot);
        edtTotalDollerValue = findViewById(R.id.edt_total_doller_value);
        txtBtcAmt = findViewById(R.id.txt_btc_value);
        edtTotalDollerValue.setClickable(false);
        edtTotalDollerValue.setEnabled(false);

        btnCharge = findViewById(R.id.btn_charge);
        imgWallet = findViewById(R.id.wallet);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        progressBar = findViewById(R.id.progressBar);

        b0.setOnClickListener(this);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        btnDot.setOnClickListener(this);
        btnback.setOnClickListener(this);
        btnCharge.setOnClickListener(this);
        imgWallet.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);


      /*  edtTotalDollerValue.setEnabled(false);
        edtTotalDollerValue.setClickable(false);*/
    }

    private void convertDollarToBtc() {
        generalTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    if (edtTotalDollerValue.getText().hashCode() == s.hashCode()) {
                        try {

                            if (edtTotalDollerValue.getText().toString().equalsIgnoreCase("0")) {
                                txtBtcAmt.setText("0.00000000  BTC");
                            } else {
                                final double dollerAmt = Double.valueOf((String.valueOf(s)));
                                //Log.e(TAG, "afterTextChanged: " + ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
                                final float btcAmt = (float) (dollerAmt / Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));
                                //  final float btcAmt = (float) (dollerAmt / Double.valueOf(10450.80));

                                DecimalFormat numberFormat = new DecimalFormat("#0.00000000");
                                final String strBtcAmt = numberFormat.format(btcAmt);

                                txtBtcAmt.setText(strBtcAmt + "  BTC");
                              /*  String url = "https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + strBtcAmt;
                                Tools.displayImageOriginalString(context, imgBarcode, url);*/
                            }

                        } catch (Exception e) {
                            txtBtcAmt.setText("0.00000000  BTC");
                            Log.e(TAG, "onClick: cross Btn:" + e.getMessage());

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    txtBtcAmt.setText("0.00000000  BTC");
                    Log.e(TAG, "onClick: cross Btn:" + e.getMessage());
                }
            }


        };

        edtTotalDollerValue.addTextChangedListener(generalTextWatcher);

    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_0:
                if (edtTotalDollerValue.getText().toString().equalsIgnoreCase("0")) {
                    edtTotalDollerValue.setText("0");
                } else {
                    edtTotalDollerValue.setText(edtTotalDollerValue.getText().insert(edtTotalDollerValue.getText().length(), "0"));
                }
                break;
            case R.id.btn_1:
                numberBtnPressed("1");
                break;
            case R.id.btn_2:
                numberBtnPressed("2");
                break;
            case R.id.btn_3:
                numberBtnPressed("3");
                break;
            case R.id.btn_4:
                numberBtnPressed("4");
                break;
            case R.id.btn_5:
                numberBtnPressed("5");
                break;
            case R.id.btn_6:
                numberBtnPressed("6");
                break;
            case R.id.btn_7:
                numberBtnPressed("7");
                break;
            case R.id.btn_8:
                numberBtnPressed("8");
                break;
            case R.id.btn_9:
                numberBtnPressed("9");
                break;
            case R.id.btn_dot:
                if (edtTotalDollerValue.getText().toString().equalsIgnoreCase("0") ||
                        edtTotalDollerValue.getText().toString().equalsIgnoreCase("")) {
                    numberBtnPressed("0.");
                } else if (edtTotalDollerValue.getText().toString().substring(edtTotalDollerValue.length() - 1, edtTotalDollerValue.length())
                        .equalsIgnoreCase(".")) {
                    btnDot.setEnabled(false);  // no more than 1 dot in sequence
                } else {
                    numberBtnPressed(".");
                }
                break;
            case R.id.btn_back:
                try {
                    edtTotalDollerValue.setText(
                            edtTotalDollerValue.getText().delete(edtTotalDollerValue.getText().length() - 1,
                                    edtTotalDollerValue.getText().length()));
                } catch (Exception e) {
                    Log.e(TAG, "onClick: cross Btn:" + e.getMessage());
                }
                break;
            case R.id.btn_charge:
                if (Validate()) {
                    ACU.MySP.saveSP(context, ACU.MySP.CHARGED_AMT, edtTotalDollerValue.getText().toString());

                    if (ACU.MySP.getSPBoolean(context, ACU.MySP.IS_EMP_SWITCH_ON, false)) {
                        startActivity(new Intent(context, SelectEmployeeFromListActivity.class));
                    } else {
                        startActivity(new Intent(context, TipActivity.class));
                    }
                    finish();
                }
                break;
            case R.id.wallet:
                startActivity(new Intent(context, PaymentReceivedActivity.class));
                finish();
                break;
            case R.id.title_bar_left_menu:
                ACU.MySP.CHECK_FOR_ACTIVITY = "ChargeBitwalletActivity";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(context, PinforInnerActivity.class));
                        finish();
                    }
                }, SPLASH_TIME_OUT);
                //    startActivity(new Intent(context, SidePanalActivity.class));
                break;

        }
    }

    public void numberBtnPressed(String number) {
        if (!edtTotalDollerValue.getText().toString().contains(".")) {
            btnDot.setEnabled(true);
        }
        if (edtTotalDollerValue.getText().toString().equalsIgnoreCase("0")) {
            edtTotalDollerValue.setText(number);
        } else {
            edtTotalDollerValue.setText(edtTotalDollerValue.getText().insert(edtTotalDollerValue.getText().length(), number));
        }
    }


    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            Log.e(TAG, "doExitApp: onBackPressed");
            minimizeBtnPressed = true;
            finish();
        }
    }

    public boolean Validate() {

        try {
            String split = edtTotalDollerValue.getText().toString().substring(edtTotalDollerValue.length() - 1, edtTotalDollerValue.length());
            Log.e(TAG, "Validate: " + split);
            if (edtTotalDollerValue.getText().toString().equalsIgnoreCase("") ||
                    edtTotalDollerValue.getText().toString().equalsIgnoreCase("0") ||
                    edtTotalDollerValue.getText().toString().equalsIgnoreCase("00") ||
                    edtTotalDollerValue.getText().toString().equalsIgnoreCase("000") ||
                    edtTotalDollerValue.getText().toString().equalsIgnoreCase("0000") ||
                    edtTotalDollerValue.getText().toString().equalsIgnoreCase("00000") ||
                    edtTotalDollerValue.getText().toString().equalsIgnoreCase("000000")) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_USD_Amount));
                return false;

            } else if (edtTotalDollerValue.getText().toString().equalsIgnoreCase("0.")) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
                return false;
            } else if (split.equalsIgnoreCase(".")) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
                return false;
            }
        } catch (Exception e) {
        }

        return true;
    }

    public void getWalletId() {
        final String encode_emailId = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes());
        String urlParameters = "username=" + encode_emailId + "&walletType=MAIN";
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(walletUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        getWalletId();
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        Log.e("Msg", msg);

                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");
                        Log.e(TAG, "processResponse: " + status);
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");

                            JSONArray walletListArray = dataObj.getJSONArray("walletList");


                            for (int i = 0; i < walletListArray.length(); i++) {
                                JSONObject walletObj = walletListArray.getJSONObject(i);
                                int isdefault = walletObj.getInt("is_default");  //in btc
                                Log.e("walletListArray", String.valueOf(isdefault));
                                if (isdefault == 1) {
                                    Log.e("walletListArray", walletObj.getString("wallet_id"));
                                   ACU.MySP.saveSP(context, ACU.MySP.WALLET_ID, walletObj.getString("wallet_id"));
                                    ACU.MySP.saveSP(context, ACU.MySP.WALLET_BUSINESS_ID, walletObj.getString("wallet_id"));
                                    ACU.MySP.saveSP(context, ACU.MySP.WALLET_Type, walletObj.getString("walletType"));

                                }

                            }

                        } else {
                            if (c < 5) {
                                getWalletId();
                                c++;
                            }

                        }
                    }
                } catch (Exception e) {

                }
            }
        });
        restAsyncTask.execute();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isConnectingToInternet(Context appContext, String screenType) {
        // Method to check internet connection
        ConnectivityManager conMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {

            return true;
        } else {


//             Toast.makeText(appContext, "No internet connection.", Toast.LENGTH_SHORT).show();
            showCustomDialog(appContext, screenType);
            return false;
        }
    }


    public void showCustomDialog(final Context appContext, String screenType) {
        final Dialog dialog = new Dialog(appContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_warning_tab);
        } else {
            dialog.setContentView(R.layout.dialog_warning);
        }
        dialog.setCancelable(false);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                // Method to check internet connection
                ConnectivityManager conMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {
                    dialog.cancel();
                    getWalletId();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
