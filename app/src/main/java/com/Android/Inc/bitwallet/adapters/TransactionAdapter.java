package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.Activities.BuySellPopup.BuySellPopup;
import com.Android.Inc.bitwallet.Activities.SocialMediaActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    Context context;
    JSONArray jsonArray;
    String screenType;
    private static final String TAG = TransactionAdapter.class.getSimpleName();


    public TransactionAdapter(Context context, JSONArray jsonArray, String screenType) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.screenType = screenType;
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (holder instanceof MyViewHolder) {
            try {
                JSONObject dataListObj = jsonArray.getJSONObject(position);
                String strTime = dataListObj.getString("time");
                String strCategory = dataListObj.getString("category");
                final String strTxId = dataListObj.getString("txid");
                final double conformStatus = dataListObj.getDouble("confirmations");
                final double WalletAmt = dataListObj.getDouble("amount");

                final double Usdrate = Double.valueOf(ACU.MySP.getFromSP(context,ACU.MySP.USD_RATE,""));
                final float walletDollerAmt = (float) (WalletAmt * Usdrate);
                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);

                String[] strArray = strTime.split(" ");
                String strDate = strArray[0];

                holder.txtDate.setText(strDate);
                holder.txtCatagory.setText(strCategory);
                holder.txtDollerAmt.setText("$" + strWalletDollerAmt);
                String[] strDollerArray = null;
                Log.e(TAG, "onBindViewHolder: "+ strCategory);
                switch (strCategory) {
                    case "SEND":
                        holder.txtCatagory.setText("SENT");
                        Log.e(TAG, "onBindViewHolder: strWalletDollerAmt "+strWalletDollerAmt );
                       // strDollerArray = strWalletDollerAmt.split("-");
                     //   Log.e(TAG, "onBindViewHolder: strDollerArray "+strDollerArray[1]+" "+strDollerArray[0] );
                        holder.txtDollerAmt.setText("-$" + strWalletDollerAmt);
                      //  Log.e(TAG, "onBindViewHolder: txtDollerAmt "+holder.txtDollerAmt.getText().toString() );
                        checkForConformation(holder, strWalletDollerAmt, conformStatus, position);
                        break;
                    case "SELL":
                        holder.txtCatagory.setText("SELL");
                        holder.txtDollerAmt.setText("-$" + strWalletDollerAmt);
                        checkForConformation(holder, strWalletDollerAmt, conformStatus, position);
                        break;
                    case "REC":
                        holder.txtCatagory.setText("RECEIVED");
                        holder.txtDollerAmt.setText("$" + strWalletDollerAmt);
                        checkForConformation(holder, strWalletDollerAmt, conformStatus, position);
                        break;
                    case "BUY":
                        holder.txtCatagory.setText("BUY");
                        holder.txtDollerAmt.setText("$" + strWalletDollerAmt);
                        checkForConformation(holder, strWalletDollerAmt, conformStatus, position);
                        break;

                    default:
                        checkForConformation(holder, strWalletDollerAmt, conformStatus, position);
                }

         /*
         // temprory closed

                holder.txtDollerAmt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (VU.isConnectingToInternet(context, screenType)) {
                            String strTxId = "";
                            String strCategory="";
                            try {
                                strTxId = dataListObj.getString("txid");
                                 strCategory = dataListObj.getString("category");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                            Intent in_contact = new Intent(context, BuySellPopup.class);
                            in_contact.putExtra("txt_id", strTxId);
                            in_contact.putExtra("txt_type", strCategory);
                                context.startActivity(in_contact);
                        }
                    }
                });


          */
                holder.ll_viewOnBlockChain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (VU.isConnectingToInternet(context, screenType)) {
//                            String strTxId = null;
//                            try {
//                                strTxId = dataListObj.getString("txid");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
                            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                            Intent in_contact = new Intent(context, SocialMediaActivity.class);
                            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                            in_contact.putExtra("flag", "blockChain");
                            in_contact.putExtra("url", "https://blockchain.com/btc/tx/" + strTxId);
                            context.startActivity(in_contact);
                        }
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void checkForConformation(MyViewHolder holder, String strWalletDollerAmt, double conformStatus, int position) {

        Log.e(TAG, "checkForConformation: strWalletDollerAmt :"+strWalletDollerAmt+" conformStatus: "+conformStatus );
        if (strWalletDollerAmt.equalsIgnoreCase("0.00") ||strWalletDollerAmt.equalsIgnoreCase("0") ||strWalletDollerAmt.equalsIgnoreCase("0.0")) {
            holder.txtConfirmed.setText("");
            holder.txtViewOnBlockChain.setText("");
            holder.txtDollerAmt.setVisibility(View.INVISIBLE);

        } else {
            if (conformStatus > 0.0) {
                holder.txtConfirmed.setText("CONFIRMED");
            } else {
                holder.txtConfirmed.setText("PENDING");
            }
            holder.txtViewOnBlockChain.setText("VIEW BLOCKCHAIN");
            holder.txtDollerAmt.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtDollerAmt, txtDate, txtCatagory, txtViewOnBlockChain, txtConfirmed;
        LinearLayout ll_viewOnBlockChain;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtDollerAmt = (TextView) itemView.findViewById(R.id.txt_dollerAmt);
            this.txtDate = (TextView) itemView.findViewById(R.id.txt_date);
            this.txtCatagory = (TextView) itemView.findViewById(R.id.txt_category);
            this.txtViewOnBlockChain = (TextView) itemView.findViewById(R.id.txt_viewOnBlockChain);
            this.txtConfirmed = (TextView) itemView.findViewById(R.id.txt_confirmed);
            this.ll_viewOnBlockChain = (LinearLayout) itemView.findViewById(R.id.ll_viewOnBlockChain);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_transaction_tab, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_transaction, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }
}
