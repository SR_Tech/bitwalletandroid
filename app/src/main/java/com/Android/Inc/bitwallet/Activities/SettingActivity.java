package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

public class SettingActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {
    private TextView txtAccount, txtPassword, txtEmployees, txtfactorAuth, txtSession, txtAppinfo,txtFaq,txtAPiKey, txtLogout;
    private View layout;
    private Context context;
    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_drawer;
    String screenType;
    private boolean minimizeBtnPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_setting_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_setting);
        }

        context = SettingActivity.this;
        initialize();

        if (ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").equalsIgnoreCase("BUSINESS")) {
            txtEmployees.setVisibility(View.VISIBLE);
        } else {
            txtEmployees.setVisibility(View.GONE);
        }
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    public void initialize() {
        txtAccount = findViewById(R.id.txt_account);
        txtPassword = findViewById(R.id.txt_password);
        txtEmployees = findViewById(R.id.txt_Employees);
        txtfactorAuth = findViewById(R.id.txt_factAuth);
        txtSession = findViewById(R.id.txt_session);
        txtAppinfo = findViewById(R.id.txt_appInfo);
        txtFaq = findViewById(R.id.txt_faq);
        txtLogout = findViewById(R.id.txt_logout);
        txtAPiKey = findViewById(R.id.txt_getapikey);





        txtAccount.setOnClickListener(this);
        txtPassword.setOnClickListener(this);
        txtEmployees.setOnClickListener(this);
        txtfactorAuth.setOnClickListener(this);
        txtSession.setOnClickListener(this);
        txtAppinfo.setOnClickListener(this);
        txtFaq.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        txtAPiKey.setOnClickListener(this);


        ll_drawer = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_drawer.setOnClickListener(this);


        String user_type = ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "");


        if (user_type.equalsIgnoreCase("BUSINESS")) {
           txtAPiKey.setVisibility(View.VISIBLE);
          //  txtAPiKey.setVisibility(View.GONE);
        }
        else
        {
            txtAPiKey.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_account:
                //     ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "account");   // to get which fragment is click ---> settingOptionsActivity
                startActivity(new Intent(context, AccountActivity.class));
                finish();
                break;
            case R.id.txt_password:
                //    ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "password");
                startActivity(new Intent(context, PasswordActivity.class));
                finish();
                break;
            case R.id.txt_Employees:
                //    ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "password");
                //     startActivity(new Intent(context, EmployeeListActivity.class));
                startActivity(new Intent(context, EnableDisableActivity.class));
                finish();
                break;
            case R.id.txt_factAuth:
                //   ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "2factAuth");
                startActivity(new Intent(context, TwoFactAuthActivity.class));
                finish();
                break;
            case R.id.txt_session:
                //    ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "session");
                startActivity(new Intent(context, SessionActivity.class));
                finish();
                break;
            case R.id.txt_appInfo:
                //    ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "appInfo");
                startActivity(new Intent(context, AppInfoActivity.class));
                finish();
                break;
            case R.id.txt_faq:
                //    ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "appInfo");
                startActivity(new Intent(context, FaqActivity.class));
                finish();
                break;

             case R.id.txt_getapikey:
                startActivity(new Intent(context, APIKeyShareActivity.class));
                finish();
                break;

            case R.id.txt_logout:
                //    ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT, "logout");
                startActivity(new Intent(context, LogoutActivity.class));
                finish();
                break;
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }
    }
      public void socialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType))
        {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SidePanalActivity.class));
        finish();
    }
}



