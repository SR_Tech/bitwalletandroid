package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.RetrofitClient.RetrofitClient;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.EmployeeListAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeListActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerEmployeeList;
    private EmployeeListAdapter employeeListAdapter;
    private JSONArray dataArray;
    private Context context;
    private ImageView imgPlus, imgMinus;
    private ProgressBar progressBar;
    private boolean isMinusBtnClick = false;
    private LinearLayout title_bar_left_menu, llImgPlus, llImgMinus;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;

    private String screenType;
    private boolean minimizeBtnPressed = false;

    private API exampleApi;
    private AsyncTask mtaskEmployeeList = null, mtaskDeleteEmployee = null;
    public static String exampleUrl = ACU.MySP.MAIN_URL + "employeelist";
    private static final String TAG = EmployeeListActivity.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = EmployeeListActivity.this;
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_employee_list_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_employee_list);
        }
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        initRecycler();
        DevEmployeeListData();
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGrou3nd");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        swipeRefreshLayout = findViewById(R.id.emp_list_refresh);
        recyclerEmployeeList = findViewById(R.id.recycler_employee_list);
        imgPlus = findViewById(R.id.img_plus);
        imgMinus = findViewById(R.id.img_minus);
        llImgPlus = findViewById(R.id.ll_img_plus);
        llImgMinus = findViewById(R.id.ll_img_minus);
        progressBar = findViewById(R.id.emp_list_pogressbar);
        title_bar_left_menu = findViewById(R.id.title_bar_left_menu);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        llImgPlus.setOnClickListener(this);
        llImgMinus.setOnClickListener(this);
        title_bar_left_menu.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);


    }

    private void initRecycler() {
        @SuppressLint("WrongConstant")
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerEmployeeList.setLayoutManager(linearLayoutManager);
        recyclerEmployeeList.setHasFixedSize(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {

            DevEmployeeListData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_img_plus:
                startActivity(new Intent(context, AddUpdateEmployeeActivity.class));
                break;
            case R.id.ll_img_minus:
                minusBtnClick();
                break;
            case R.id.title_bar_left_menu:
                startActivity(new Intent(context, EnableDisableActivity.class));
                finish();
                break;
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", "https://www.instagram.com/bitwalletinc/");
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
        }
    }

    private void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    private void minusBtnClick() {
        if (isMinusBtnClick) {
            isMinusBtnClick = false;
        } else {
            isMinusBtnClick = true;
        }
        employeeListAdapter = new EmployeeListAdapter(context, dataArray, isMinusBtnClick);
        recyclerEmployeeList.setAdapter(employeeListAdapter);
        employeeListAdapter.setOnItemClickListener(new EmployeeListAdapter.RecyclerViewItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemClick(View view, int position) {
                try {
                    String bussinessEmail = dataArray.getJSONObject(position).getString("buseremail");
                    String Email = dataArray.getJSONObject(position).getString("email");
                    String empId = dataArray.getJSONObject(position).getString("empId");
                    String empName = dataArray.getJSONObject(position).getString("firstName");
                    deleteWalletConfirmDialog(VU.encode(bussinessEmail.getBytes()), VU.encode(Email.getBytes()), VU.encode(empId.getBytes()));
                } catch (Exception e) {
                    Log.e(TAG, "onItemClick: " + e.getMessage());
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, EnableDisableActivity.class));
        finish();

        super.onBackPressed();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevEmployeeListData() {
        progressBar.setVisibility(View.VISIBLE);
        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzIwOTAzMzksInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczNTYxNTY4fQ.l4p8RqyPCBmv6CDz6erXeCWQU0TEZzAR4Pe02k9rqWwOSFh3aNP1xlEL8iTu6FOFba19i9XFugYCW718iSEBSA";
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().EmployeeList(auth_token, "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                    if (Obj != null && statusCode.equalsIgnoreCase("200")) {
                        dataArray = Obj.getJSONArray("data");
                        adapterCall();
                    } else {
                        dialogEmployeeListActivity(strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void adapterCall() {

        employeeListAdapter = new EmployeeListAdapter(context, dataArray, false);
        recyclerEmployeeList.setAdapter(employeeListAdapter);
    }

    public void dialogEmployeeListActivity(final String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    // Employee list
    private void EmpployeeList() {
        progressBar.setVisibility(View.VISIBLE);

        mtaskEmployeeList = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null;
                try {
                    exampleApi = new API(context);
                    s = exampleApi.doGet(exampleUrl, "EmployeeList");
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        dialogEmployeeListActivity("Request Timeout.Please Try Again");
                    } else {
                        try {
                            String api_response = s;
                            Log.e(TAG, "onResponse: " + api_response);
                            JSONObject Obj = new JSONObject(api_response);
                            String statusCode = Obj.getString("status_code");
                            String strMessage = Obj.getString("message");
                            if (Obj != null && statusCode.equalsIgnoreCase("200")) {
                                dataArray = Obj.getJSONArray("data");
                                adapterCall();
                            } else {
                                dialogEmployeeListActivity(strMessage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //    CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    dialogEmployeeListActivity("Request Timeout.Please Try Again");
                }
            }
        }.execute();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevDeleteEmployeeData(String encoded_bussinessEmail, String encoded_email, String encoded_empId) {

        Log.e(TAG, "DevDeleteEmployeeData: " + encoded_bussinessEmail + " " + encoded_email + " " + encoded_empId);
        progressBar.setVisibility(View.VISIBLE);
        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzIwOTAzMzksInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczNTYxNTY4fQ.l4p8RqyPCBmv6CDz6erXeCWQU0TEZzAR4Pe02k9rqWwOSFh3aNP1xlEL8iTu6FOFba19i9XFugYCW718iSEBSA";
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteEmployee(auth_token, encoded_empId, encoded_email, encoded_bussinessEmail);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                    dialogEmployeeListActivity(strMessage);
                    if (Obj != null && statusCode.equalsIgnoreCase("200")) {
                        DevEmployeeListData();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: catch " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void deleteWalletConfirmDialog(final String encoded_bussinessEmail, final String encoded_email, final String encoded_empId) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_delete_wallet_tab);
        } else {
            dialog.setContentView(R.layout.dialog_delete_wallet);
        }
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (VU.isConnectingToInternet(context, screenType)) {
                    dialog.dismiss();
                    DevDeleteEmployeeData(encoded_bussinessEmail, encoded_email, encoded_empId);
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    // delete Employee
    private void DeleteEmployeeData(final String encoded_bussinessEmail, final String encoded_email, final String encoded_empId) {
        progressBar.setVisibility(View.VISIBLE);

        mtaskEmployeeList = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null;
                try {
                    exampleApi = new API(context, encoded_empId, encoded_email, encoded_bussinessEmail);
                    s = exampleApi.doGet(exampleUrl, "deleteEmployee");
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        dialogEmployeeListActivity("Request Timeout.Please Try Again");
                    } else {
                        try {
                            String api_response = s;
                            Log.e(TAG, "onResponse: " + api_response);
                            JSONObject Obj = new JSONObject(api_response);
                            String statusCode = Obj.getString("status_code");
                            String strMessage = Obj.getString("message");
                            dialogEmployeeListActivity(strMessage);
                            if (Obj != null && statusCode.equalsIgnoreCase("200")) {
                                EmpployeeList();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: catch " + e.getMessage());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //    CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    dialogEmployeeListActivity("Request Timeout.Please Try Again");
                }
            }
        }.execute();

    }


    @Override
    public void onDestroy() {
        if (mtaskEmployeeList != null && mtaskEmployeeList.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskEmployeeList.cancel(true);
        }
        if (mtaskDeleteEmployee != null && mtaskDeleteEmployee.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskDeleteEmployee.cancel(true);
        }
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        super.onDestroy();
    }

}
