package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Android.Inc.bitwallet.Activities.PinActivity;
import com.Android.Inc.bitwallet.Activities.SendingCurrencyActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.AllImagesAdapter;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.ImageUtils;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensooq.supernova.gligar.GligarPicker;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class UploadPhotoActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver {

    private Context context;
    private String screenType;
    private int PICKER_REQUEST_CODE = 30;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 12;
    private static final String TAG = UploadPhotoActivity.class.getSimpleName();
    private String docType, pdfStr, strWyreAccId, selectedDocType;  //selectedDocType : image or pdf
    private ViewPager imageViewPager;
    private AllImagesAdapter allImagesAdapter;
    private Button btnChooseFile, btnUploadFile;
    private PDFView pdfView;
    private ImageView imgDefault;
    private ProgressBar progressBar;
    private LinearLayout ll_backArrow;
    private boolean minimizeBtnPressed = false;
    private boolean isShare = false;
    private static final String pdfFileName = "wyre_doc.pdf";
    public static String upload_document_url = "wyre/upload_document";
    private String strIndividualProof = "", strProofOfAddress = "";

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_photo);

        context = UploadPhotoActivity.this;
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_upload_photo_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_upload_photo);
        }
        init();
        getIntentData();

        int currentApiVersion = Build.VERSION.SDK_INT;

        if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                //   Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
            } else {
                requestPermission();
            }
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        Log.e(TAG, "onUserLeaveHint: " );
        minimizeBtnPressed = true;

        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: " );
        if (isShare){
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        }
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void init() {
        imageViewPager = findViewById(R.id.all_images_viewpager);
        btnChooseFile = findViewById(R.id.btn_choose_photo);
        btnUploadFile = findViewById(R.id.btn_upload_photo);
        pdfView = findViewById(R.id.pdfView);
        imgDefault = findViewById(R.id.img_default);
        progressBar = findViewById(R.id.progressBar);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);

        btnUploadFile.setOnClickListener(this);
        btnChooseFile.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            docType = bundle.getString("docType");
            strWyreAccId = bundle.getString("wyreAccId");
        }

    }


    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                Log.e("grantResult", grantResults[0] + "" + "" + PackageManager.PERMISSION_DENIED);
                if (grantResults.length > 0) {

                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean storageDenied = grantResults[0] == PackageManager.PERMISSION_DENIED;

                    if (storageDenied) {
                        requestPermission();
                    } else if (storageAccepted) {
                        //  Toast.makeText(getApplicationContext(), "Permission Granted, Now You Can Access Storage", Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void inPdfConversion(String imageePath[]) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + pdfFileName));
            document.open();
            for (int i = 0; i < imageePath.length; i++) {
                Image image = Image.getInstance(ImageUtils.compressImage(imageePath[i]));  // Change image's name and extension.
                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin() - 0) / image.getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
                image.scalePercent(scaler);
                image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                document.add(image);
            }
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG, "onActivityResult: resultcode" + resultCode);
        if (resultCode != 0) {
            switch (requestCode) {
                case 30: {
                    final String pathsList[] = data.getExtras().getStringArray(GligarPicker.IMAGES_RESULT); // return list of selected images paths.
                    Log.e(TAG, "onActivityResult: image : " + Arrays.toString(pathsList));
                    if (pathsList.length > 0) {
                        ArrayList<String> imageList = new ArrayList<>();
                        for (int i = 0; i < pathsList.length; i++) {
                            imageList.add(Miscellaneous.CustomsMethods.getBase64FromPath(pathsList[i]));
                        }
                        allImagesAdapter = new AllImagesAdapter(context, imageList);
                        imageViewPager.setAdapter(allImagesAdapter);
                        imageViewPager.setVisibility(View.VISIBLE);
                        pdfView.setVisibility(View.INVISIBLE);
                        imgDefault.setVisibility(View.GONE);
                        showImageOnScreen(pathsList);
                    }

                    break;
                }

                case Constant.REQUEST_CODE_PICK_FILE: {
                    try {
                        ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                        Log.e(TAG, "onActivityResult: pdf : " + list.get(0).getPath());
                        //compressPdf(list.get(0).getPath(),list.get(0).getPath());
                        pdfStr = Miscellaneous.CustomsMethods.getBase64FromPath(list.get(0).getPath());
                        showFileIntoPdfView(pdfStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showImageOnScreen(String pathsList[]) {
        if (pathsList.length > 0) {
            if (pathsList.length > 1 && docType.equals("passport")) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_passort_msg));
            } else if (pathsList.length == 1 && docType.equals("driver_license")) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_driver_license_msg));
            } else if (pathsList.length == 1 && docType.equals("identity_card")) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_identity_card_msg));
            } else {
                inPdfConversion(pathsList);
            }
        } else {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.please_select_document));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private void showFileIntoPdfView(String pdfStr) {
        pdfView.fromBytes(Base64.decode(pdfStr, Base64.DEFAULT))
                .defaultPage(1)
                .enableAnnotationRendering(true)
                .scrollHandle(new DefaultScrollHandle(context))
                .spacing(10)
                .load();

        pdfView.setVisibility(View.VISIBLE);
        imageViewPager.setVisibility(View.INVISIBLE);
        imgDefault.setVisibility(View.GONE);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
            case R.id.btn_choose_photo:
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                isShare = true;
                documentTypeDialog();
                break;
            case R.id.btn_upload_photo:
                Log.e(TAG, "onClick: upload btn click");
                String strPdfbase64 = null;
                try {
                    if (selectedDocType.equalsIgnoreCase("IMAGE")) {
                        String filePath = Environment.getExternalStorageDirectory() + "/" + pdfFileName;  // by default we r creating pdf by given pdf file name
                        //compressPdf(filePath,filePath);
                        strPdfbase64 = Miscellaneous.CustomsMethods.getBase64FromPath(filePath);
                    } else {
                        strPdfbase64 = pdfStr;
                    }
                    if (VU.isConnectingToInternet(context, screenType))
                        uploadDocument(strPdfbase64);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    private void documentTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"IMAGE", "PDF"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(this);
            docTypeAlert.setTitle("Select Document Type");

            docTypeAlert.setItems(doc_type_list, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectedDocType = doc_type_list[which];

                    //handle logic here
                    if (selectedDocType.equals("IMAGE")) {
                        try {
                            new GligarPicker().limit(2).requestCode(PICKER_REQUEST_CODE).withActivity(UploadPhotoActivity.this).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (selectedDocType.equals("PDF")) {
                        Intent intent4 = new Intent(UploadPhotoActivity.this, NormalFilePickActivity.class);
                        intent4.putExtra(Constant.MAX_NUMBER, 1);
                        intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"pdf"});
                        startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);

                    }
                }
            });

            docTypeAlert.create().show();
        } catch (NullPointerException e) {
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private void uploadDocument(String strPdfbase64) {
        Log.e(TAG, "uploadDocument: strPdfbase64: " + strPdfbase64);
        if (docType.equals("passport") || docType.equals("driver_license")) {
            strProofOfAddress = "";   // if passport & driver_license : proof of address is blank and vice versa for next condition
            strIndividualProof = strPdfbase64;
        } else if (docType.equals("identity_card")) {
            strProofOfAddress = strPdfbase64;
            strIndividualProof = "";
        }

        String urlParameters = "individualGovernmentId=" + strIndividualProof +
                "&individualProofOfAddress=" + strProofOfAddress + "&accountId=" + strWyreAccId;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(upload_document_url);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status && status_code.equals("200")) {
                            dialogGoToSellActivity(msg);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }


    public void dialogGoToSellActivity(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(context, SellActivity.class));
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, SellActivity.class));
        finish();
    }
}
