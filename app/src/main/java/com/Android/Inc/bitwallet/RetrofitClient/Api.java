package com.Android.Inc.bitwallet.RetrofitClient;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {


    // String  auth = RetrofitClient.authCode;

    //login
   /* @FormUrlEncoded
    @POST("bit.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> login(
            @Field("username") String username,
            @Field("password") String Password);


    @POST("wyre/sell/paymentmethodlist")
    @Headers("auth_token: dddw")
    Call<ResponseBody> paymentMenthod();


    @FormUrlEncoded
    @POST("business/sendreceipt")
    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    Call<ResponseBody> receipt(
            @Field("businessName") String businessName,
            @Field("billAmt") String billAmt,
            @Field("tipAmt") String tipAmt,
            @Field("receipentEmail") String receipentEmail,
            @Field("timeZone") String timeZone,
            @Field("txHash") String txHash,
            @Field("senderAddr") String senderAddr,
            @Field("receiverAddr") String receiverAddr,
            @Field("timestamp") String timestamp);


    //registration
    @FormUrlEncoded
    @POST("register.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> registration(
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("user_name") String user_name,   // email  id
            @Field("email_id") String email_id,
            @Field("password") String Password,
            @Field("mobile_no") String mobile_no);


    //verifiy OTP
    @FormUrlEncoded
    @POST("outh.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> verifiyOTP(
            @Field("otp") String otp,
            @Field("username") String username,  // email id
            @Field("type") String type);


    // resend OTP
    @FormUrlEncoded
    @POST("resend_otp.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> resendOTP(
            @Field("user_id") String emailId);// email id


    //check Pin
    @FormUrlEncoded
    @POST("has_passcode.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> checkPin(                            // first time check for pin - blank security code
                                                            @Field("security_code") String security_code,
                                                            //   @Header("auth_token") String Header);        // dynamic header
                                                            @Field("auth_token") String authToken);


    //create and verify Pin
    @FormUrlEncoded
    @POST("passcode_verify.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> createVerifyPin(
            //   @Header("auth_token") String Header,
            @Field("security_code") String security_code,
            @Field("auth_token") String authToken);


    //forget password
    @FormUrlEncoded
    @POST("forget.php")
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    Call<ResponseBody> forgetPassword(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authToken,
            @Field("username") String emailId);


    //for demo
    @GET("BTCUSD?period=alltime&format=json")
    @Headers("X-ba-key: YjIyNjc4MTE1ZjFjNDc2MWEwMWE2YmVhMjk1YTAwYjg")
    Call<ResponseBody> demoChart();


    //portfolio and walletList Fragment
    @FormUrlEncoded
    @POST("wallet_list.php")
    Call<ResponseBody> portfolioWallets(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken);

    //create Wallet
    @FormUrlEncoded
    @POST("create_wallet.php")
    Call<ResponseBody> createWallet(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("wallet_type") String wallet_type,
            @Field("label") String wallet_name);

    //rename Wallet
    @FormUrlEncoded
    @POST("wallet_rename.php")
    Call<ResponseBody> renameWallet(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("wallet_id") String wallet_type,
            @Field("label") String wallet_name);


    //delete Wallet
    @FormUrlEncoded
    @POST("wallet_delete.php")
    Call<ResponseBody> deleteWallet(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("wallet_id") String wallet_type);


    //Support fragment
    @FormUrlEncoded
    @POST("support_query_new.php")
    Call<ResponseBody> support(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("message") String message);


    //Account Fragment
    @FormUrlEncoded
    @POST("profile_get.php")
    Call<ResponseBody> account_profile_get(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken);


    //Account Fragment
    @FormUrlEncoded
    @POST("profile_set.php")
    Call<ResponseBody> account_profile_set(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("Phone_no") String Phone_no);


    //Password Fragment
    @FormUrlEncoded
    @POST("password.php")
    Call<ResponseBody> passwordReset(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("old_pass") String oldPassword,
            @Field("new_pass") String newPassword);


    //Session Fragment
    @FormUrlEncoded
    @POST("session_list.php")
    Call<ResponseBody> sessionList(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken,
            @Field("page_index") String pageIndex,
            @Field("page_size") String pageSize);

    //Session Fragment
    @FormUrlEncoded
    @POST("session_out_all.php")
    Call<ResponseBody> logoutAllSessions(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken);

    //logout Fragment
    @FormUrlEncoded
    @POST("logout.php")
    Call<ResponseBody> logout(
            //   @Header("auth_token") String Header,
            @Field("auth_token") String authtoken);


    //Transaction Fragment
    @FormUrlEncoded
    @POST("wallet_transactions.php")
    Call<ResponseBody> transaction(
            //      @Headers("auth_token: YjIyNjc4MTE1ZjFjNDc2MWEwMWE2YmVhMjk1YTAwYjg"),
            @Field("wallet_id") String WalletId,
            @Field("auth_token") String auth_token);


    //Send Currency Fragment
    @FormUrlEncoded
    @POST("use_max.php")
    Call<ResponseBody> UseMax(
            //      @Headers("auth_token: YjIyNjc4MTE1ZjFjNDc2MWEwMWE2YmVhMjk1YTAwYjg"),
            @Field("auth_token") String auth_token,
            @Field("wallet_id") String WalletId,
            @Field("amount") String amount);


    //Send Currency Fragment
    @FormUrlEncoded
    @POST("get_fees.php")
    Call<ResponseBody> sendCurrency(
            //      @Headers("auth_token: YjIyNjc4MTE1ZjFjNDc2MWEwMWE2YmVhMjk1YTAwYjg"),
            @Field("auth_token") String auth_token,
            @Field("wallet_id") String WalletId,
            @Field("amount") String amount,
            @Field("send_to") String send_to);  //address hash key


    //Request Currency Fragment
    @FormUrlEncoded
    @POST("receive.php")
    Call<ResponseBody> RequestAddressKey(
            //      @Headers("auth_token: YjIyNjc4MTE1ZjFjNDc2MWEwMWE2YmVhMjk1YTAwYjg"),
            @Field("auth_token") String auth_token,
            @Field("wallet_id") String WalletId,
            @Field("amount") double amount);


    //Sending Currency Fragment
    @FormUrlEncoded
    @POST("send_txn.php")
    Call<ResponseBody> SendingCurrency(
            //      @Headers("auth_token: YjIyNjc4MTE1ZjFjNDc2MWEwMWE2YmVhMjk1YTAwYjg"),
            @Field("auth_token") String auth_token,
            @Field("raw_hex") String raw_hex,
            @Field("wallet_txt_id") String wallet_txt_id);


    //Chart fragment   ChartDaily
    @FormUrlEncoded
    @POST("chart_daily.php")
    Call<ResponseBody> ChartDaily(
            @Field("auth_token") String auth_token);


    //Chart fragment   ChartWeekly
    @FormUrlEncoded
    @POST("chart_weekly.php")
    Call<ResponseBody> ChartWeekly(
            @Field("auth_token") String auth_token);


    //Chart fragment   ChartMonthly
    @FormUrlEncoded
    @POST("chart_monthly.php")
    Call<ResponseBody> ChartMonthly(
            @Field("auth_token") String auth_token);


    //Chart fragment   ChartYearly
    @FormUrlEncoded
    @POST("chart_yearly.php")
    Call<ResponseBody> ChartYearly(
            @Field("auth_token") String auth_token);


    //Chart fragment   ChartAllTime
    @FormUrlEncoded
    @POST("chart_alltime.php")
    Call<ResponseBody> ChartAllTime(
            @Field("auth_token") String auth_token);


    //Chart fragment   USD rate
    @FormUrlEncoded
    @POST("usd_rate.php")
    Call<ResponseBody> ChartUSD(
            @Field("auth_token") String auth_token);

*/
    //Add Employee data
    @FormUrlEncoded
    @POST("business/employeeadd")
    Call<ResponseBody> AddEmployee(
            @Header("auth_token") String auth_token,
            @Field("firstName") String firstName,
            @Field("lastName") String lastName,
            @Field("email") String email,
            @Field("contactNo") String contactNo,
            @Field("employeePin") String employeePin,
            @Field("buseremail") String buserid,
            @Field("countryCode") String countryCode);


    @FormUrlEncoded
    @POST("business/employeelist")
    Call<ResponseBody> EmployeeList(
            @Header("auth_token") String auth_token,
            @Field("vinay") String v);


    @FormUrlEncoded
    @POST("business/employeedelete")
    Call<ResponseBody> deleteEmployee(
            @Header("auth_token") String auth_token,
            @Field("empId") String empId,
            @Field("email") String email,
            @Field("buseremail") String buseremail);

    @FormUrlEncoded
    @POST("business/employeeupdate")
    Call<ResponseBody> EmployeeUpdate(
            @Header("auth_token") String auth_token,
            @Field("firstName") String firstName,
            @Field("lastName") String lastName,
            @Field("contactNo") String contactNo,
            @Field("countryCode") String countryCode,
            @Field("buseremail") String buseremail,
            @Field("email") String empEmail,
            @Field("empId") String empId);

    @FormUrlEncoded
    @POST("business/emp_tx_tiphistory")
    Call<ResponseBody> EmployeeData(
            @Header("auth_token") String auth_token,
            @Field("username") String username,
            @Field("empemail") String empemail,
            @Field("empcode") String empcode,
            @Field("userType") String userType);

    @FormUrlEncoded
    @POST("business/paymnt_wallet_history")
    Call<ResponseBody> TransactionListData(
            @Header("auth_token") String auth_token,
            @Field("username") String username,
            @Field("userType") String empemail);

}
