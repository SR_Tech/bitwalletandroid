package com.Android.Inc.bitwallet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.github.barteksc.pdfviewer.PDFView;

public class FaqActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    Context context;
    PDFView faq;
    String screenType;
    private LinearLayout imgleftArrow;
    private boolean minimizeBtnPressed =false;
   // private static final String url = "https://www.bitwallet.org/faq/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_faq_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_faq);
        }

        context = FaqActivity.this;
        intitialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    private void intitialize() {
        imgleftArrow = findViewById(R.id.title_bar_left_menu);
        imgleftArrow.setOnClickListener(this);
     //   faq =  findViewById(R.id.pdfView);
      //  faq.fromAsset("bitwallet_faq.pdf").load();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }

    }


    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {

        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }

    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");
        // ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        // finish();

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    @Override
    public void onBackPressed() {
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        super.onBackPressed();
        startActivity(new Intent(context, SettingActivity.class));
        finish();
    }

}
