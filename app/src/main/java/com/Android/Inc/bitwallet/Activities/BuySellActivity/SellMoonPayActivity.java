package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.VU;

public class SellMoonPayActivity extends AppCompatActivity {

    private WebView webView;
    private Context context;
    // private ProgressBar progressBar;
    private boolean minimizeBtnPressed = false;
    private LinearLayout backArrow;
    private String walletType, strAmt;
    private String strMoonPayUrl = "https://sell-staging.moonpay.io?apiKey=pk_test_Z2QGjktpjiVedOI4ETad2UoxbHd2nn&baseCurrencyCode=btc&baseCurrencyAmount=0.00002525&refundWalletAddress=tb1qst9rvjnhym6kwmdkwgfs4h5dtp7cau5346jp9x&quoteCurrencyCode=usd&email=rakesh.ramteke@tissatech.com&externalTransactionId=e001158a-87-466f-967e3eb44eb6&kycAvailable=true&lockAmount=true&externalCustomerId=0d643e8f-2e45-4447-889f-c4fc1098a7e9";
    private String screenType;
    private static int SPLASH_TIME_OUT = 40000;

    private static final String TAG = MoonPayWidgitActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_sell_moon_pay);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_sell_moon_pay);
        }

        context = SellMoonPayActivity.this;
        webView = findViewById(R.id.webView);
        backArrow = findViewById(R.id.title_bar_left_menu);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);

        if (VU.isConnectingToInternet(context,screenType)){getChromeUrl();}
    }


    private void getChromeUrl() {
        webView.setVisibility(View.VISIBLE);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);

        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode(0);
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.loadUrl(strMoonPayUrl);

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                Log.e(TAG, "shouldOverrideUrlLoading: "+view.getUrl() );
                return super.shouldOverrideUrlLoading(view, request);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.e(TAG, "onPageStarted: "+url+"\n"+view.getUrl() );
            }
        });
    }
}
