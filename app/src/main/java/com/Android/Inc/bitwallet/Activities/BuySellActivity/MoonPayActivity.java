package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.Activities.AccountActivity;
import com.Android.Inc.bitwallet.Activities.ChartActivity;
import com.Android.Inc.bitwallet.Activities.PortfolioActivity;
import com.Android.Inc.bitwallet.Activities.SocialMediaActivity;
import com.Android.Inc.bitwallet.Activities.TransactionActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.VU;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MoonPayActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressBar;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    private View bottom_sheet;
    private LinearLayout backArrow;
    private List<String> currencyTypeList;
    TextWatcher generalTextWatcher = null;
    private int previousLength;
    private boolean backSpace, typing;
    private boolean minimizeBtnPressed = false;
    private EditText edtCurrencyType, edtDollarAmt, edtBtcAmt;
    private Context context;
    private String screenType, strMoonPayUrl;
    private static final String TAG = MoonPayActivity.class.getSimpleName();
    TextView txt_fragmentName;
    String txttype;
    TextView txt_note;

    public static final String Get_Moon_pay_url = "moonpay/get_widget_url";
    public static final String Get_Moon_pay_urlnew = "moonpay/v2/get_widget_url";
    public static String API_GET_SELL_FEES = "crypto-currency/get-fees";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_moon_pay_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_moon_pay);
        }
        init();
        conversionText();
        currencyTypeList = new ArrayList<>();
        currencyTypeList.add("USD");
        context = MoonPayActivity.this;


       /* if (VU.isConnectingToInternet(context, screenType)) {
            getMoonPayWidgetUrl();
        }*/

        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

    }

    private void init() {
        //get the current intent


        //get the attached extras from the intent
        //we should use the same key as we used to attach the data.\

        txt_fragmentName = findViewById(R.id.txt_fragmentName);
        txt_note = findViewById(R.id.txt_note);
        txttype = getIntent().getStringExtra("txt_type");

        if (txttype.equals("BUY")) {
            txt_fragmentName.setText("BUY BITCOIN");
            txt_note.setVisibility(View.VISIBLE);
        } else {
            txt_note.setVisibility(View.GONE);
            txt_fragmentName.setText("SELL BITCOIN");
        }
        Log.d(TAG, "getMoonPayWidgetUrl: " + txttype);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        Button btn = findViewById(R.id.btn_submit);
        edtCurrencyType = findViewById(R.id.edt_currency);
        edtDollarAmt = findViewById(R.id.edt_dollar_amt);
        edtBtcAmt = findViewById(R.id.edt_btc_amt);
        progressBar = findViewById(R.id.progressBar);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

        edtCurrencyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: walletType");
                showBottomSheetDialog();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VU.isConnectingToInternet(context, screenType))
                    if (validate()) {

                        if(txttype.equals("SELL"))
                        {
                            getSellTransactionFee();
                        }
                        else
                        {
                        getMoonPayWidgetUrl();
                        }
                    }
            }
        });
    }


    private void getSellTransactionFee()
    {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(API_GET_SELL_FEES);
        helper.setUrlParameter("");
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "processResponse: " + jsonObject.toString());
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        String status = jsonObject.getString("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equals("200") && status.equals("true")) {
                          String  transactionfees = jsonObject.getString("data");
                            Log.e(TAG, "transactionfees: " + transactionfees);

                            Double doubleFees = Double.valueOf((String.valueOf(transactionfees)));

                            Double enteredBTC = Double.valueOf(edtBtcAmt.getText().toString().replace(",", ""));
                            Double trnsactionBtc = doubleFees + enteredBTC;
                            String wallet_btc = ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "");
                            final double double_wallet_btc = Double.valueOf((String.valueOf(wallet_btc)));
                            //    print(trnsactionBtc)
                            if (double_wallet_btc >= trnsactionBtc)
                            {
                                getMoonPayWidgetUrl();
                            }
                            else
                            {

                                Double eligibleblnce = double_wallet_btc - doubleFees;
                                double eligibleUSD = (double) ((eligibleblnce * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                final String streligibleUSD = numberFormat.format(eligibleUSD);
                                CustomDialogs.dialogShowMsg(context, screenType, "You Are Eligible To Perform Sell Only For $"+streligibleUSD);
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (
                        Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();
    }

    private void getMoonPayWidgetUrl() {
        //
        String urlParameters;
//        if (txttype.equals("BUY")) {
//            Log.d(TAG, "getMoonPayWidgetUrlvvv: " + txttype);
//            urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.IS_DEFAULT_WALLET_ID, "") + "&currency_code=" + "USD" + "&currency_amt=" + edtDollarAmt.getText().toString().replace(",", "") + "&txn_type=" + txttype;
//            Log.d(TAG, "hhh: " + edtDollarAmt.getText().toString().replace(",", ""));
//        } else {
//            Log.d(TAG, "getMoonPayWidgetUrlfff: " + txttype);
//            String strBtcAMt = edtBtcAmt.getText().toString().replace(",", "");
//            // String btcAmt = Miscellaneous.CustomsMethods.convertDollerToBtc(strBtcAMt,context);
//            Log.d(TAG, "getMoonPayWidgetUrlfff: " + strBtcAMt);
//
//            urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.IS_DEFAULT_WALLET_ID, "") + "&currency_code=" + "USD" + "&currency_amt=" + strBtcAMt + "&txn_type=" + txttype;
//        }

        if (txttype.equals("BUY")) {
            Log.d(TAG, "getMoonPayWidgetUrlvvv: " + txttype);
            urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&currency_code=" + "USD" + "&currency_amt=" + edtDollarAmt.getText().toString().replace(",", "") + "&txn_type=" + txttype;
            Log.d(TAG, "hhh: " + edtDollarAmt.getText().toString().replace(",", ""));
        } else {
            Log.d(TAG, "getMoonPayWidgetUrlfff: " + txttype);
            String strBtcAMt = edtBtcAmt.getText().toString().replace(",", "");
            // String btcAmt = Miscellaneous.CustomsMethods.convertDollerToBtc(strBtcAMt,context);
            Log.d(TAG, "getMoonPayWidgetUrlfff: " + strBtcAMt);
            urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&currency_code=" + "USD" + "&currency_amt=" + strBtcAMt + "&txn_type=" + txttype;
        }



        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(Get_Moon_pay_urlnew);
        helper.setUrlParameter(urlParameters);

        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "processResponse: " + jsonObject.toString());
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        String status = jsonObject.getString("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equals("200") && status.equals("true")) {
                            strMoonPayUrl = jsonObject.getJSONObject("data").getString("widget_url");
                            startActivity(new Intent(context, MoonPayWidgitActivity.class)
                                    .putExtra("walletType", edtCurrencyType.getText().toString())
                                    .putExtra("amt", edtDollarAmt.getText().toString().replace(",", ""))
                                    .putExtra("moonPayUrl", strMoonPayUrl));
                            finish();
                        } else if (status_code.equals("401")) {
                            dialogGoToAccountActivity(msg);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (
                        Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();

    }

    public boolean validate() {
//        if (edtCurrencyType.getText().toString().equalsIgnoreCase("")) {
//            CustomDialogs.dialogShowMsg(context, screenType, "Select Currency Type");
//            return false;
//        }
//        else
        if (edtDollarAmt.getText().toString().equalsIgnoreCase("")) {
            CustomDialogs.dialogShowMsg(context, screenType, "Enter Dollar Amount");
            return false;
        } else if (edtBtcAmt.getText().toString().equalsIgnoreCase("")) {
            CustomDialogs.dialogShowMsg(context, screenType, "Enter BTC Amount");
            return false;
        } else if (txttype.equals("BUY")) {
            if ((Double.valueOf(edtDollarAmt.getText().toString().replace(",", ""))) <= 29) {
                CustomDialogs.dialogShowMsg(context, screenType, "Amount Should Be Minimum $30");
                return false;
            }
        }
        else if (txttype.equals("SELL"))
        {

            String wallet_btc = ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "");
            final double double_wallet_btc = Double.valueOf((String.valueOf(wallet_btc)));

            Log.e(TAG, "double_wallet_btc: "+double_wallet_btc );
            if (double_wallet_btc < 0.00100) {

                CustomDialogs.dialogShowMsg(context, screenType, "You Don't Have Sufficient Balance To Perform Sell");
                return false;
            } else if ((Double.valueOf(edtBtcAmt.getText().toString().replace(",", ""))) < 0.00100) {
                final double btcAmt = Double.valueOf((String.valueOf(0.00100)));
                double walletDollerAmt = (double) ((btcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                CustomDialogs.dialogShowMsg(context, screenType, "Amount should be greater than $"+strWalletDollerAmt);
                return false;
            }
           else if ((Double.valueOf(edtBtcAmt.getText().toString().replace(",", "")))  > 0.02) {
                final double btcAmt = Double.valueOf((String.valueOf(0.02)));
                double walletDollerAmt = (double) ((btcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                CustomDialogs.dialogShowMsg(context, screenType, "Amount should be less than $"+strWalletDollerAmt);
                return false;
            }
        }
        return true;
    }


    private void showBottomSheetDialog() {
        Log.e(TAG, "showBottomSheetDialog: paymentNameList: 1" + currencyTypeList);
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
        {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        Log.e(TAG, "showBottomSheetDialog: paymentNameList:2 " + currencyTypeList);
        final View view = getLayoutInflater().inflate(R.layout.wallet_bottom_list, null);
        final ListView currencyTypeList = view.findViewById(R.id.list_bottom);
        Log.e(TAG, "showBottomSheetDialog: paymentNameList: 3" + this.currencyTypeList);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.adapter_wallet_bottom_list, R.id.textView, this.currencyTypeList);
        currencyTypeList.setAdapter(arrayAdapter);
        currencyTypeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                edtCurrencyType.setText(MoonPayActivity.this.currencyTypeList.get(position));
                mBottomSheetDialog.cancel();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", "https://www.instagram.com/bitwalletinc/");
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

        }
    }

    public void socialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public void dialogGoToAccountActivity(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn_tab);
        } else {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);
        dialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "MoonpayActivity");
//                startActivity(new Intent(context, AccountActivity.class));
//                finish();
                Intent intent = new Intent(context, AccountActivity.class);
                intent.putExtra("txt_type", txttype);
                startActivity(intent);
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    public void conversionText() {
        generalTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                previousLength = s.length();
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    backSpace = previousLength > s.length();

                    if (edtBtcAmt.getText().hashCode() == s.hashCode()) {
                        typing = true;
                        try {
                            //  edtBtcAmt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
                            edtDollarAmt.removeTextChangedListener(generalTextWatcher);

                            if (edtBtcAmt.length() == 0) {
                                edtDollarAmt.setText("");
                            } else {

                                final double btcAmt = Double.valueOf((String.valueOf(s)));
                                Log.e("btcAmt", btcAmt + "");
                                Log.e("usd", ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
                                double walletDollerAmt = (double) ((btcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                                Log.e("walletDollerAmt", walletDollerAmt + "");

                                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                Log.e("strWalletDollerAmt", strWalletDollerAmt);
                              /*  if(strWalletDollerAmt.length()>3)
                                String[] str_arr1 = strWalletDollerAmt.split(".");
                                Log.e("str_arr1", str_arr1[0]);
                                if(str_arr1[0].equalsIgnoreCase("0")){
                                    edtAmtDoller.setText("0"+strWalletDollerAmt);
                                }else {
                                    edtAmtDoller.setText(strWalletDollerAmt);
                                }*/
                                edtDollarAmt.setText(strWalletDollerAmt);
                                ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, strWalletDollerAmt);
                            }
                            edtDollarAmt.addTextChangedListener(generalTextWatcher);
                        } catch (Exception e) {
                            e.printStackTrace();
                            // CustomToast.custom_Toast(contextDashBoard, "incorrect input", layout);
                            edtDollarAmt.addTextChangedListener(generalTextWatcher);
                        }
                    } else if (edtDollarAmt.getText().hashCode() == s.hashCode()) {
                        // edtDollarAmt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        if (typing) {
                            if (backSpace) {
                                edtDollarAmt.setText("");
                                edtBtcAmt.setText("");
                            }
                            typing = false;
                        }
                        edtBtcAmt.removeTextChangedListener(generalTextWatcher);
                        if (edtDollarAmt.length() == 0) {
                            edtBtcAmt.setText("");
                        } else {
                            final double dollerAmt = Double.valueOf((String.valueOf(s)));
                            final float btcAmt = (float) (dollerAmt / Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));

                            DecimalFormat numberFormat = new DecimalFormat("#0.00000000");
                            final String strBtcAmt = numberFormat.format(btcAmt);
                            edtBtcAmt.setText(strBtcAmt);

                        }

                        edtBtcAmt.addTextChangedListener(generalTextWatcher);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    edtBtcAmt.addTextChangedListener(generalTextWatcher);
                    //dialogSendCurrencyActivity("Please Provide Valid Input");
                }
            }
        };
        edtDollarAmt.addTextChangedListener(generalTextWatcher);
        edtBtcAmt.addTextChangedListener(generalTextWatcher);
    }


    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: " + ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, ""));
        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("Portfolio_sendReceive")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, PortfolioActivity.class));
            finish();
        } else if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("TransactionActivity")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, TransactionActivity.class));
            finish();
        } else if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("charts_sendReceive")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, ChartActivity.class));
            finish();
        }

//        else {
//            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
//            startActivity(new Intent(context, PortfolioActivity.class));
//            finish();
//        }
    }

}
