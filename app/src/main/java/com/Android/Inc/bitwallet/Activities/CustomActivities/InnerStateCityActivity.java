package com.Android.Inc.bitwallet.Activities.CustomActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.Android.Inc.bitwallet.Activities.PinActivity;
import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.adapters.InnerSearchListAdapter;
import com.Android.Inc.bitwallet.adapters.SearchListAdapter;
import com.Android.Inc.bitwallet.utils.ACU;

import java.util.ArrayList;
import java.util.List;

public class InnerStateCityActivity extends AppCompatActivity implements  LifecycleObserver {

    private Context context;
    Activity activity;
    LinearLayout ll_backArrow;
    private static final String TAG = StateCityActivity.class.getSimpleName();

    List<String> NameList;
    ListView listView;
    SearchView searchView;
    InnerSearchListAdapter adapter;
    private List<ListItemModel> listItemListModel;
    private boolean minimizeBtnPressed = false;
    String screenType,searchType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_state_city_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_state_city);
        }
        context = InnerStateCityActivity.this;
        activity = InnerStateCityActivity.this;
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        getIntentData();
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        ll_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getIntentData(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            NameList = bundle.getStringArrayList("name_list");
            searchType = bundle.getString("search_type");
        }

    }

    private void initialize() {
        listView = findViewById(R.id.list_view);
        searchView = findViewById(R.id.search);
        listItemListModel = new ArrayList<>();

        for (int i = 0; i < NameList.size(); i++) {
            ListItemModel listItemModel = new ListItemModel();
            listItemModel.setName(NameList.get(i));

            listItemListModel.add(listItemModel);

        }

        adapter = new InnerSearchListAdapter(listItemListModel, context);
        listView.setAdapter(adapter);

        searchView.setActivated(true);
        searchView.setQueryHint("Select Your "+searchType);
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    adapter.filter("");
                    listView.clearTextFilter();

                } else {
                    adapter.filter(newText);
                }

                return true;
            }
        });

    }

    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {

        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }
    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");
      /*  if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        }else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }*/
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");
        // ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        // finish();

        /*if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }*/

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: ");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        super.onBackPressed();
    }
}
