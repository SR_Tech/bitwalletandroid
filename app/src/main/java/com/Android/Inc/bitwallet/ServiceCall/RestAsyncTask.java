package com.Android.Inc.bitwallet.ServiceCall;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;

import org.json.JSONObject;


public class RestAsyncTask extends AsyncTask<String, String, String> {

    private Context context;
    private RestAPIClientHelper helper;
    private String response;
    private String TAG;
    public AsyncResponse asyncResponse;

    public RestAsyncTask(Context context, RestAPIClientHelper helper, String TAG, AsyncResponse asyncResponse) {
        this.context = context;
        this.helper = helper;
        this.TAG = TAG;
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((Activity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            response = RestAPIClient.APICLient.getRemoteCall(helper, context);
            Log.e(TAG, "doInBackground: response: " + response);
        } catch (Exception e) {
            Log.e(TAG, "doInBackground: catch : " + e.getMessage());

            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        ((Activity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        asyncResponse.processResponse(response);
    }
}