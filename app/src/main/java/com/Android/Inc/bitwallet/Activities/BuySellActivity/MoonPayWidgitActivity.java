package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.Android.Inc.bitwallet.Activities.ChartActivity;
import com.Android.Inc.bitwallet.Activities.PortfolioActivity;
import com.Android.Inc.bitwallet.Activities.TransactionActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MoonPayWidgitActivity extends AppCompatActivity {
    private WebView webView;
    private Context context;
    // private ProgressBar progressBar;
    private boolean minimizeBtnPressed = false;
    private LinearLayout backArrow;
    private String walletType, strAmt, strMoonPayUrl;
    private String screenType;
    private static int SPLASH_TIME_OUT = 40000;

    /*-- CUSTOMIZE --*/
    /*-- you can customize these options for your convenience --*/
    private static String file_type = "image/*";    // file types to be allowed for upload
    private boolean multiple_files = true;         // allowing multiple file upload

    /*-- MAIN VARIABLES --*/
    private String cam_file_data = null;        // for storing camera file information
    private ValueCallback<Uri> file_data;       // data/header received after file selection
    private ValueCallback<Uri[]> file_path;     // received file(s) temp. location

    private final static int file_req_code = 1;

    private PermissionRequest mPermissionRequest;
/*
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String[] PERM_CAMERA =
            {Manifest.permission.CAMERA};*/

    private static final String TAG = MoonPayWidgitActivity.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_moon_pay_widget);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_moon_pay_widget);
        }
        context = MoonPayWidgitActivity.this;
        webView = findViewById(R.id.webView);
        backArrow = findViewById(R.id.title_bar_left_menu);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        try {
            getIntentData();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        getChromeUrl();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getIntentData() throws NoSuchAlgorithmException, InvalidKeyException {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strAmt = bundle.getString("amt");
            walletType = bundle.getString("walletType").toLowerCase();
            strMoonPayUrl = bundle.getString("moonPayUrl");

           //strMoonPayUrl = "https://buy-staging.moonpay.io?apiKey=pk_test_Z2QGjktpjiVedOI4ETad2UoxbHd2nn&currencyCode=btc&walletAddress=35x3CVPQXojzXzLGt61rhQ4Z56XKnPZizE&externalTransactionId=04ce6a84-747a-476f-860a-b22c77a4ac48&externalCustomerId=2b065061-d779-45a1-863a-f0b52f727578&baseCurrencyAmount=30&baseCurrencyCode=usd&kycAvailable=true&lockAmount=true&email=latachandekar786@gmail.com&redirectURL=http://3.132.74.72:8099/rest/moonpay/buy/transaction";
         //  strMoonPayUrl = "https://buy-staging.moonpay.io?apiKey=pk_test_Z2QGjktpjiVedOI4ETad2UoxbHd2nn&currencyCode=btc&walletAddress=tb1qafdz7f5vzd4v0n9z5njuu8lcelshjdpzytjv39&externalTransactionId=1cf9fc71-3722-41e4-b1b9-ed088e7e6065&email=vinay.chandekar@tissatech.com&kycAvailable=true&lockAmount=true&externalCustomerId=586666ad-6b4b-4a0a-a911-20fb4939bef4&redirectURL=http://3.132.74.72:8099/rest/moonpay/buy/transaction&baseCurrencyAmount=25&baseCurrencyCode=USD";
            Log.e(TAG, "getIntentData: strBuyWidgetUrl: " + strMoonPayUrl);
        }
    }

    private void  getChromeUrl() {
        webView.setVisibility(View.VISIBLE);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);

        /*webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);*/


        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode(0);
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.setWebViewClient(new Callback());
        webView.loadUrl(strMoonPayUrl);
        webView.setWebChromeClient(new WebChromeClient() {
            // Need to accept permissions to use the camera

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                Log.e(TAG, "onProgressChanged: " + view.getUrl());
                String url = view.getUrl();
                String status = "";
                // String status = url.split("\\=")[2];
                if (url.contains("pending")) {
                    status = "pending";
                } else if (url.contains("failed")) {
                    status = "failed";
                } else if (url.contains("succcess")) {
                    status = "succcess";
                }
                Log.e(TAG, "onProgressChanged: status: " + status);
                switch (status) {
                          /*  case "pending":
                                Dialog dialogPending = CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.moon_pay_status_pending));
                                dialogPending.findViewById(R.id.btn_ok).setOnClickListener(v -> {
                                    dialogPending.dismiss();
                                    finish();
                                });

                                break;*/
                    case "failed":
                        Dialog dialogFail = CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.moon_pay_status_fail));
                        dialogFail.findViewById(R.id.btn_ok).setOnClickListener(v -> {
                            dialogFail.dismiss();
                            finish();
                        });

                        break;
                           /* case "succcess":
                                Dialog dialogSucess = CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.moon_pay_status_success));
                                dialogSucess.findViewById(R.id.btn_ok).setOnClickListener(v -> {
                                    dialogSucess.dismiss();
                                    finish();
                                });

                                break;*/
                }
            }

            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.e(TAG, "onPermissionRequest");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.getResources());
                }

                //setCameraPermission(request);
            }

            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {

                if (file_permission() && Build.VERSION.SDK_INT >= 21) {
                    file_path = filePathCallback;
                    Intent takePictureIntent = null;
                    Intent takeVideoIntent = null;

                    boolean includeVideo = false;
                    boolean includePhoto = false;

                    /*-- checking the accept parameter to determine which intent(s) to include --*/
                    paramCheck:
                    for (String acceptTypes : fileChooserParams.getAcceptTypes()) {
                        String[] splitTypes = acceptTypes.split(", ?+"); // although it's an array, it still seems to be the whole value; split it out into chunks so that we can detect multiple values
                        for (String acceptType : splitTypes) {
                            switch (acceptType) {
                                case "*/*":
                                    includePhoto = true;
                                    includeVideo = true;
                                    break paramCheck;
                                case "image/*":
                                    includePhoto = true;
                                    break;
                                case "video/*":
                                    includeVideo = true;
                                    break;
                            }
                        }
                    }

                    if (fileChooserParams.getAcceptTypes().length == 0) {   //no `accept` parameter was specified, allow both photo and video
                        includePhoto = true;
                        includeVideo = true;
                    }

                    if (includePhoto) {
                        takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(MoonPayWidgitActivity.this.getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                photoFile = create_image();
                                takePictureIntent.putExtra("PhotoPath", cam_file_data);
                            } catch (IOException ex) {
                                Log.e(TAG, "Image file creation failed", ex);
                            }
                            if (photoFile != null) {
                                cam_file_data = "file:" + photoFile.getAbsolutePath();
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            } else {
                                cam_file_data = null;
                                takePictureIntent = null;
                            }
                        }
                    }

                    if (includeVideo) {
                        takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        if (takeVideoIntent.resolveActivity(MoonPayWidgitActivity.this.getPackageManager()) != null) {
                            File videoFile = null;
                            try {
                                videoFile = create_video();
                            } catch (IOException ex) {
                                Log.e(TAG, "Video file creation failed", ex);
                            }
                            if (videoFile != null) {
                                cam_file_data = "file:" + videoFile.getAbsolutePath();
                                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile));
                            } else {
                                cam_file_data = null;
                                takeVideoIntent = null;
                            }
                        }
                    }

                    Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                    contentSelectionIntent.setType(file_type);
                    if (multiple_files) {
                        contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    }

                    Intent[] intentArray;
                    if (takePictureIntent != null && takeVideoIntent != null) {
                        intentArray = new Intent[]{takePictureIntent, takeVideoIntent};
                    } else if (takePictureIntent != null) {
                        intentArray = new Intent[]{takePictureIntent};
                    } else if (takeVideoIntent != null) {
                        intentArray = new Intent[]{takeVideoIntent};
                    } else {
                        intentArray = new Intent[0];
                    }

                    Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                    chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                    chooserIntent.putExtra(Intent.EXTRA_TITLE, "File chooser");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                    startActivityForResult(chooserIntent, file_req_code);
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    /*-- callback reporting if error occurs --*/
    public class Callback extends WebViewClient {
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getApplicationContext(), "Failed loading app!", Toast.LENGTH_SHORT).show();
        }
    }

    /*-- checking and asking for required file permissions --*/
    public boolean file_permission() {
        if (Build.VERSION.SDK_INT >= 23 && (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(MoonPayWidgitActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
            return false;
        } else {
            return true;
        }
    }

    /*-- creating new image file here --*/
    private File create_image() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    /*-- creating new video file here --*/
    private File create_video() throws IOException {
        @SuppressLint("SimpleDateFormat")
        String file_name = new SimpleDateFormat("yyyy_mm_ss").format(new Date());
        String new_name = "file_" + file_name + "_";
        File sd_directory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(new_name, ".3gp", sd_directory);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Build.VERSION.SDK_INT >= 21) {
            Uri[] results = null;

            /*-- if file request cancelled; exited camera. we need to send null value to make future attempts workable --*/
            if (resultCode == Activity.RESULT_CANCELED) {
                if (requestCode == file_req_code) {
                    file_path.onReceiveValue(null);
                    return;
                }
            }

            /*-- continue if response is positive --*/
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == file_req_code) {
                    if (null == file_path) {
                        return;
                    }

                    ClipData clipData;
                    String stringData;
                    try {
                        clipData = intent.getClipData();
                        stringData = intent.getDataString();
                    } catch (Exception e) {
                        clipData = null;
                        stringData = null;
                    }

                    if (clipData == null && stringData == null && cam_file_data != null) {
                        results = new Uri[]{Uri.parse(cam_file_data)};
                    } else {
                        if (clipData != null) { // checking if multiple files selected or not
                            final int numSelectedFiles = clipData.getItemCount();
                            results = new Uri[numSelectedFiles];
                            for (int i = 0; i < clipData.getItemCount(); i++) {
                                results[i] = clipData.getItemAt(i).getUri();
                            }
                        } else {
                            results = new Uri[]{Uri.parse(stringData)};
                        }
                    }
                }
            }
            file_path.onReceiveValue(results);
            file_path = null;
        } else {
            if (requestCode == file_req_code) {
                if (null == file_data) return;
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                file_data.onReceiveValue(result);
                file_data = null;
            }
        }
    }

    public void dialogTransactionFail() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(context.getResources().getString(R.string.transaction_failed));

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void setCameraPermission(PermissionRequest request){
        mPermissionRequest = request;
        final String[] requestedResources = request.getResources();
        for (String r : requestedResources) {
            if (r.equals(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                // In this sample, we only accept video capture request.
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
                        .setTitle("Allow Permission to camera")
                        .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mPermissionRequest.grant(new String[]{PermissionRequest.RESOURCE_VIDEO_CAPTURE});
                                Log.d(TAG,"Granted");
                            }
                        })
                        .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mPermissionRequest.deny();
                                Log.d(TAG,"Denied");
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e(TAG, "onBackPressed: " + ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, ""));
        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("Portfolio_sendReceive")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, PortfolioActivity.class));
            finish();
        } else if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("TransactionActivity")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, TransactionActivity.class));
            finish();
        } else if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("charts_sendReceive")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, ChartActivity.class));
            finish();
        }
    }


}
